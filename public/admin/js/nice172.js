$(function(){
	$(document).on('click', 'th input:checkbox' , function(){
		var that = this;
		$(this).closest('table').find('tr > td:first-child input:checkbox').each(function(){
			this.checked = that.checked;
			$(this).closest('tr').toggleClass('selected');
		});
	});
	
	$('.ajaxForm').submit(function(){
			var _this = $(this);
			var url = _this.attr('action');
			var index = '';
			$(this).ajaxSubmit({
				type : 'POST',
				url : url,
				beforeSend : function(e){
					index = layer.load(1, {
					  	shade: [0.2,'#333']
					});
				},
				success : function(data){
					if(typeof data == 'string'){
						var data = $.parseJSON(data);
					}
					if(data.reset == 1){_this.resetForm();}
					if(data.reload == 1 && data.url != undefined){
						window.location.href = data.url;
					}else{
						window.location.reload();
					}
					layer.msg(data.msg, function(){});
					layer.close(index);
				},
			});
		return false;
	});
		
	//通用修改状态
	$('.ajaxStatus').click(function(){
		var _this = $(this);
		var action = $(this).attr('action');
		var id = $(this).attr('data-id');
		$.ajax({
			type:'post',
			url:action,
			data : {id : id},
			success : function(data){
				if(typeof data == 'string'){
					var data = $.parseJSON(data);
				}
				if(data.code == 1){
					if(data.reload == 1){
						window.location.reload();
						return;
					}
					if(data.status){
						_this.removeClass('btn-warning');
					}else{
						_this.removeClass('btn-success');
					}
					_this.addClass(data.classText);
					_this.text(data.msg);
				}else{
					layer.msg(data.msg, function(){});
				}
			}
		});
	});
		
	$('.ajaxDelete').click(function(){
		if(!confirm('确认要删除吗？')) return false;
		var _this = $(this);
		var action = _this.attr('action');
		$.ajax({
			type:'get',
			url:action,
			data:{},
			success: function(data){
				if(typeof data == 'string'){var data = $.parseJSON(data);}
				if(data.code == 1){
					if(data.reload == 1){
						window.location.reload();
						return;
					}
					$('#tr'+_this.attr('data-id')).remove();
				}else{layer.msg(data.msg, function(){});}
			}
		});
	});
	
	$('#del_grid-table').click(function(){
		if(!confirm('确认要删除吗？')) return false;
		var ids = '';
		var action = $(this).attr('action');
		$('#sample-table-1').find('tr > td:first-child input:checkbox').each(function(){
			if(this.checked){
				ids += $(this).val()+'|';
			}
		});
		if(ids == '') return false;
		$.ajax({
			type:'post',
			url:action,
			data:{ids:ids},
			success: function(data){
				if(typeof data == 'string'){var data = $.parseJSON(data);}
				if(data.code == 1){
					window.location.reload();
				}else{layer.msg(data.msg, function(){});}
			}
		});
	});

$("#file0").change(function () {
    var objUrl = getObjectURL(this.files[0]);
    console.log("objUrl = " + objUrl);
    if (objUrl) {
        $("#img0").attr("src", objUrl);
    }
});
$("#file1").change(function () {
    var objUrl = getObjectURL(this.files[0]);
    console.log("objUrl = " + objUrl);
    if (objUrl) {
        $("#img1").attr("src", objUrl);
    }
});
$("#file2").change(function () {
    var objUrl = getObjectURL(this.files[0]);
    console.log("objUrl = " + objUrl);
    if (objUrl) {
        $("#img2").attr("src", objUrl);
    }
});
$("#file3").change(function () {
    var objUrl = getObjectURL(this.files[0]);
    console.log("objUrl = " + objUrl);
    if (objUrl) {
        $("#img3").attr("src", objUrl);
    }
});
$("#file4").change(function () {
    var objUrl = getObjectURL(this.files[0]);
    console.log("objUrl = " + objUrl);
    if (objUrl) {
        $("#img4").attr("src", objUrl);
    }
});
function getObjectURL(file) {
    var url = null;
    if (window.createObjectURL != undefined) { // basic
        $("#oldcheckpic").val("nopic");
        url = window.createObjectURL(file);
    } else if (window.URL != undefined) { // mozilla(firefox)
        $("#oldcheckpic").val("nopic");
        url = window.URL.createObjectURL(file);
    } else if (window.webkitURL != undefined) { // webkit or chrome
        $("#oldcheckpic").val("nopic");
        url = window.webkitURL.createObjectURL(file);
    }
    return url;
}
		
});

function backpic(picurl) {
    $("#img0").attr("src", picurl);//还原修改前的图片
    $("input[name='file0']").val("");//清空文本框的值
    $("input[name='oldcheckpic']").val(picurl);//清空文本框的值
}

function backpic(picurl) {
    $("#img1").attr("src", picurl);//还原修改前的图片
    $("input[name='file1']").val("");//清空文本框的值
    $("input[name='oldcheckpic1']").val(picurl);//清空文本框的值
}

function backpic2(picurl) {
    $("#img2").attr("src", picurl);//还原修改前的图片
    $("input[name='file2']").val("");//清空文本框的值
    $("input[name='oldcheckpic2']").val(picurl);//清空文本框的值
}

function backpic3(picurl) {
    $("#img3").attr("src", picurl);//还原修改前的图片
    $("input[name='file3']").val("");//清空文本框的值
    $("input[name='oldcheckpic3']").val(picurl);//清空文本框的值
}
function backpic4(picurl) {
    $("#img4").attr("src", picurl);//还原修改前的图片
    $("input[name='file4']").val("");//清空文本框的值
    $("input[name='oldcheckpic4']").val(picurl);//清空文本框的值
}