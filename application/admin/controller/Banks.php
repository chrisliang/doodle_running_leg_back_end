<?php 
namespace app\admin\controller;
use think\Db;
use think\Controller;
/**
* 银行控制器
*/
class Banks extends Common
{
	private $db = null;
	private $config = '';
	private $default_config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $userField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->default_config = config('database');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
	}

	/**
	 * [index 银行列表]
	 * @return [type] [description]
	 */
	public function index(){
		$m = $this->db->name('banks');
		$listobj = $m->where('bankFlag=1')->order('bankId desc')->paginate(10);
		$listarr = $listobj->toArray();
		$pager = $listobj->render();
		$list = $listarr['data'];
		// $list['page'] = $pager;
		// dump($list);
		// exit();
		$this->assign('page',$pager);
		$this->assign('list',$list);
		return $this->fetch('list');
	}

	/**
	 * [toEdit 跳转到修改/添加页面]
	 * @return [type] [description]
	 */
	public function toEdit(){
		$m = $this->db->name('banks');
		$id = input('id/d');
		if($id>0){
			$item = $m->where('bankId='.$id)->find();
			$this->assign('item',$item);
			return $this->fetch('edit');
		}else{
			return $this->fetch('add');
		}
	}

	/**
	 * [add 添加银行卡]
	 * @return [json] [ajax表达提交]
	 */
	public function add(){
		$m = $this->db->name('banks');
		$bankName = input('bankName/s');
		$data['bankName'] = $bankName;
		if($m->insert($data)){
			$result = array('code' => 1,'reload' => 1,'url' => url('banks/index'),'msg' => '添加成功');
		}else{
			$result = array('code' => 0,'reload' => 0,'url' => url('banks/index'),'msg' => '添加失败');
		}
		return json($result);
	}

	/**
	 * [edit 修改银行卡信息]
	 * @return [json] [ajax表达提交]
	 */
	public function edit(){
		$m = $this->db->name('banks');
		$id = input('id/d');
		$bankName = input('bankName/s');
		$data['bankName'] = $bankName;
		if($m->where(['bankId'=>$id])->update($data)){
			$result = array('code' => 1,'reload' => 1,'url' => url('banks/index'),'msg' => '修改成功');
		}else{
			$result = array('code' => 0,'reload' => 1,'url' => url('banks/index'),'msg' => '修改失败');
		}
		return json($result);
	}

	/**
	 * [del 删除银行卡]
	 * @return [json] [description]
	 */
	public function del(){
		$m = $this->db->name('banks');
		$id = input('id/d');
		if($m->where(['bankId'=>$id])->delete()){
			$result = ['status'=>1,'msg'=>'删除成功','id'=>$id];
		}else{
			$result = ['status'=>0,'msg'=>'删除失败'];
		}
		return json($result);
	}

}
