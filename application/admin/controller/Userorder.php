<?php
namespace app\admin\controller;
use think\Db;
use think\Controller;
/**
* 用户订单控制器
*/
class Userorder extends Common
{
	/**
	 * [lists 用户订单列表]
	 * @return [type] [description]
	 */
	public function index(){
		$m = db('send_order');
		$id = input('id',0);
		$username = input('username');
		$order_status = input('order_status',-1);
		$pageParam    = ['query' =>[]];
		$map = array();
		if($id>0){
			$m->where('o.id ='.$id);
			$pageParam['query']['id'] = $id;
		}
		if(!empty($username)){
			$m->where('o.username='.$username);
			$pageParam['query']['username'] = $username;
		}
		if($order_status!='-1'){
			if(empty($order_status))$order_status=0;
			$m->where('o.order_status='.$order_status);
			$pageParam['query']['order_status'] = $order_status;
		}
		// 不需要联表
		// $listobj = $m
		// ->alias('o')
		// ->join('delivery_order d','d.orderId=o.id')
		// ->order('o.time desc')
		// ->field('o.*,d.id as did, d.orderId,d.get_goods_time,d.require_time,d.deliverMoney,d.userId')
		// ->paginate(10,false,$pageParam);
		$listobj = $m->alias('o')->order('o.time desc')->paginate(10,false,$pageParam);


		$page = $listobj->render();
		$listarr = $listobj->toArray();
		$list = $listarr['data'];
		if(!empty($list)){
			foreach($list as $k =>$v){
				$list[$k]['money'] = $v['money']+$v['extra_money'];
				//订单内容
				$list[$k]['content'] = json_decode($v['content'],true);
				//支付方式
				switch ($v['pay_type']) {
					case '0':
						$list[$k]['paytype'] = '余额支付';
						break;
					case '1':
						$list[$k]['paytype'] = '支付宝支付';
						break;
					case '2':
						$list[$k]['paytype'] = '微信支付';
						break;
					default:
						$list[$k]['paytype'] = '其他支付';
						break;
				}
				//支付状态
				switch ($v['pay_status']) {
					case '1':
						$list[$k]['paystatus'] = '已付款';
						break;
					case '2':
						$list[$k]['paystatus'] = '退款';
						break;
					case '3':
						$list[$k]['paystatus'] = '退款成功';
						break;
					case '4':
						$list[$k]['paystatus'] = '拒绝退款';
						break;
					default:
						$list[$k]['paystatus'] = '未知状态';
						break;
				}
				//订单状态
				switch ($v['order_status']) {
					case '0':
						$list[$k]['orderstatus'] = '待抢单';
						break;
					case '1':
						$list[$k]['orderstatus'] = '已接单未取货';
						break;
					case '2':
						$list[$k]['orderstatus'] = '配送中';
						break;
					case '3':
						$list[$k]['orderstatus'] = '配送完成';
						break;
					case '5':
						$list[$k]['orderstatus'] = '用户取消';
						break;
					default:
						$list[$k]['orderstatus'] = '未知状态';
						break;
				}
			}
		}
		// dump($page);
		// dump($list);
		$this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('id',input('id',''));
		$this->assign('username',input('username',''));
		$this->assign('order_status',input('order_status','-1'));
		return $this->fetch('lists');
	}

	/**
	 * [detail 订单详情]
	 * @return [json] [description]
	 */
	public function detail(){
		$this->assign('not_layout',true);
		$orderId = $this->request->param('id');
		if (!$orderId) $this->error('订单不存在');
		$orderInfo = db('send_order')
		->where(array('id' => $orderId))
		->find();
		$orderInfo = db('send_order')
		->alias('o')
		->join('delivery_order d','d.orderId=o.id')
		->join('user u','u.id=d.userId')
		->where('o.id='.$orderId)
		->field('o.*,d.id as did, d.orderId,d.get_goods_time,d.require_time,d.deliverMoney,d.userId,u.truename as deliveryUserName,u.mobile as deliveryMobile')
		->find();
		if (!$orderInfo) $this->error('订单不存在');
		$orderInfo['content'] = json_decode($orderInfo['content'],true);
		$orderInfo['money'] = $orderInfo['money'] + $orderInfo['extra_money'];
		switch ($orderInfo['content']['type']){
			case 1:
				$orderInfo['content']['type_text'] = '帮我送';
				break;
			case 2:
				$orderInfo['content']['type_text'] = '帮我取';
				break;
			case 3:
				$orderInfo['content']['type_text'] = '帮我买';
				break;
		}
		switch ($orderInfo['content']['pay_type']) {
			case '0':
				$orderInfo['content']['paytype'] = '余额支付';
				break;
			case '1':
				$orderInfo['content']['paytype'] = '支付宝支付';
				break;
			case '2':
				$orderInfo['content']['paytype'] = '微信支付';
				break;
			default:
				$orderInfo['content']['paytype'] = '其他支付';
				break;
		}
		//支付状态
		switch ($orderInfo['pay_status']) {
			case '1':
				$orderInfo['paystatus'] = '已付款';
				break;
			case '2':
				$orderInfo['paystatus'] = '退款';
				break;
			case '3':
				$orderInfo['paystatus'] = '退款成功';
				break;
			case '4':
				$orderInfo['paystatus'] = '拒绝退款';
				break;
			default:
				$orderInfo['paystatus'] = '未知状态';
				break;
		}
		//订单状态
		switch ($orderInfo['order_status']) {
			case '0':
				$orderInfo['orderstatus'] = '待抢单';
				break;
			case '1':
				$orderInfo['orderstatus'] = '已接单未取货';
				break;
			case '2':
				$orderInfo['orderstatus'] = '配送中';
				break;
			case '3':
				$orderInfo['orderstatus'] = '配送完成';
				break;
			case '5':
				$orderInfo['orderstatus'] = '用户取消';
				break;
			default:
				$orderInfo['orderstatus'] = '未知状态';
				break;
		}
		// dump($orderInfo);
		$this->assign('orderInfo',$orderInfo);
		if(input('op')=='refund'){
			return $this->fetch('detail_refund');
		}
		return $this->fetch();
	}

	/**
	 * [list_refund 用户退款订单列表]
	 * @return [type] [description]
	 */
	public function list_refund(){
		$m = db('send_order');
		$id = input('id',0);
		$username = input('username');
		$pay_status = input('pay_status',0);
		$pageParam    = ['query' =>[]];
		$map = array();
		if($id>0){
			$m->where('o.id ='.$id);
			$pageParam['query']['id'] = $id;
		}
		if(!empty($username)){
			$m->where('o.username='.$username);
			$pageParam['query']['username'] = $username;
		}
		if($pay_status!=0){
			$m->where('o.pay_status='.$pay_status);
			$pageParam['query']['pay_status'] = $pay_status;
		}
		$m->where('o.pay_status>1');
		$listobj = $m
		->alias('o')
		->join('delivery_order d','d.orderId=o.id')
		->order('o.time desc')
		->field('o.*,d.id as did, d.orderId,d.get_goods_time,d.require_time,d.deliverMoney,d.userId')
		->paginate(config('PAGE_SIZE'));

		$page = $listobj->render();
		$listarr = $listobj->toArray();
		$list = $listarr['data'];
		if(!empty($list)){
			foreach($list as $k =>$v){
				//订单内容
				$list[$k]['content'] = json_decode($v['content'],true);
				//支付方式
				switch ($v['pay_type']) {
					case '0':
						$list[$k]['paytype'] = '余额支付';
						break;
					case '1':
						$list[$k]['paytype'] = '支付宝支付';
						break;
					case '2':
						$list[$k]['paytype'] = '微信支付';
						break;
					default:
						$list[$k]['paytype'] = '其他支付';
						break;
				}
				//支付状态
				switch ($v['pay_status']) {
					case '1':
						$list[$k]['paystatus'] = '已付款';
						break;
					case '2':
						$list[$k]['paystatus'] = '退款成功';
						break;
					case '3':
						$list[$k]['paystatus'] = '退款成功';
						break;
					case '4':
						$list[$k]['paystatus'] = '拒绝退款';
						break;
					default:
						$list[$k]['paystatus'] = '未知状态';
						break;
				}
				//订单状态
				switch ($v['order_status']) {
					case '0':
						$list[$k]['orderstatus'] = '待抢单';
						break;
					case '1':
						$list[$k]['orderstatus'] = '已接单未取货';
						break;
					case '2':
						$list[$k]['orderstatus'] = '配送中';
						break;
					case '3':
						$list[$k]['orderstatus'] = '配送完成';
						break;
					case '5':
						$list[$k]['orderstatus'] = '用户取消';
						break;
					default:
						$list[$k]['orderstatus'] = '未知状态';
						break;
				}
			}
		}
		// dump($list);
		$this->assign('list',$list);
		$this->assign('page',$page);
		$this->assign('id',input('id',''));
		$this->assign('username',input('username',''));
		$this->assign('pay_status',input('pay_status',0));
		return $this->fetch('list_refund');
	}


}