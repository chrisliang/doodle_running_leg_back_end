<?php
namespace app\admin\controller;

class Issue extends Common {
	
	public function index(){
		$lists = db('issue')->select();
		$this->assign('lists',$lists);
		$this->assign('page','');
		return $this->fetch();
	}
	
	public function add(){
		return $this->fetch();
	}
	
	public function edit(){
		$id = intval(input('id'));
		$data = db('issue')->where(array('id' => $id))->find();
		if (empty($data)) $this->error('数据不存在');
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	public function delete(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		if (db('issue')->where(array('id' => $id))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
		}
	}
	
	public function run_edit(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = array(
			'id' => input('post.id'),
			'title' => input('post.title'),
			'type' => input('post.type'),
		);
		if (empty($data['title'])) $this->ajaxReturn(array('code' => 0,'msg' => '标题不能为空'));
		if (db('issue')->update($data)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'url' => url('index'),'msg' => '编辑成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '编辑成功'));
		}
	}
	
	public function run_add(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = array(
			'title' => input('post.title'),
			'type' => input('post.type'),
		);
		if (empty($data['title'])) $this->ajaxReturn(array('code' => 0,'msg' => '标题不能为空'));
		if (db('issue')->insert($data)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'url' => url('index'),'msg' => '编辑成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '编辑成功'));
		}
	}
	
}