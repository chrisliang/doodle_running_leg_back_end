<?php
namespace app\admin\controller;
class Manager extends Common {
	
	public function index(){
		$lists = db('admin as a')->join('__AUTH_GROUP__ g','a.group_id=g.id' ,'LEFT')->field("a.*,g.title as groupname")->paginate(config('PAGE_SIZE'));
		$this->assign('page',$lists->render());
		$this->assign('lists',$lists);
		return $this->fetch();
	}
	
	public function deleteadmin(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		if (db('admin')->where(array('id' => $id))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
		}
	}
	
	public function add_admin(){
		$lists = db('auth_group')->where(array('status' => 1))->select();
		$this->assign('lists',$lists);
		return $this->fetch();
	}
	
	public function add_admin_handler(){
		$data = $this->check_admin_post();
		$data['time'] = time();
		$model = db('admin');
		if ($model->where(array('username' => $data['username']))->find()) $this->ajaxReturn(array('code' => 0,'msg' => '用户名已存在'));
		if ($model->where(array('email' => $data['email']))->find()) $this->ajaxReturn(array('code' => 0,'msg' => '邮箱已存在'));
		if ($model->where(array('mobile' => $data['mobile']))->find()) $this->ajaxReturn(array('code' => 0,'msg' => '手机号码已存在'));
		$data['passsalt'] = create_salt();
		$data['password'] = optimizedSaltPwd($data['password'], $data['passsalt']);
		if($model->insert($data)){
			$uid = $model->getLastInsID();
			db('auth_group_access')->insert(array('uid' => $uid,'group_id' => $data['group_id']));
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'url' => url('Manager/index'),'msg' => '新增成功'));
		} else{
			$this->error('新增失败');
		}
	}
	
	private function check_admin_post(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = array(
			'username' => input('post.username'),
			'password' => input('post.password'),
			'truename' => input('post.truename'),
			'email' => input('post.email'),
			'mobile' => input('post.mobile'),
			'group_id' => input('post.group_id'),
			'status' => input('post.status'),
		);
		if (empty($data['username'])) $this->ajaxReturn(array('code' => 0,'msg' => '用户名不能为空'));
		$id = intval(input('post.id'));
		if (!$id || !empty($data['password'])){
			if (!(mb_strlen($data['password'],'utf-8') >= 6 && mb_strlen($data['password']) <= 20)) $this->ajaxReturn(array('code' => 0,'msg' => '密码长度6到20位'));
		}
		if (empty($data['truename'])) $this->ajaxReturn(array('code' => 0,'msg' => '真实姓名不能为空'));
		if (empty($data['email'])) $this->ajaxReturn(array('code' => 0,'msg' => '邮箱不能为空'));
		if (empty($data['mobile'])) $this->ajaxReturn(array('code' => 0,'msg' => '手机号码不能为空'));
		if (!_checkemail($data['email'])) $this->ajaxReturn(array('code' => 0,'msg' => '邮箱格式不正确'));
		if (!_checkmobile($data['mobile'])) $this->ajaxReturn(array('code' => 0,'msg' => '手机号码格式不正确'));
		return $data;
	}
	
	public function edit_admin(){
		$id = intval(input('id'));
		if (!$id) $this->error('操作失败');
		$data = db('admin')->where(array('id' => $id))->find();
		if (empty($data)) $this->error('数据不存在');
		$lists = db('auth_group')->where(array('status' => 1))->select();
		$this->assign('lists',$lists);
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	public function edit_admin_handler(){
		$data = $this->check_admin_post();
		$id = intval(input('post.id'));
		$data['id'] = $id;
		$model = db('admin');
		if ($model->where(array('id' => array('NEQ',$data['id']),'username' => $data['username']))->find()) $this->ajaxReturn(array('code' => 0,'msg' => '用户名已存在'));
		if ($model->where(array('id' => array('NEQ',$data['id']),'email' => $data['email']))->find()) $this->ajaxReturn(array('code' => 0,'msg' => '邮箱已存在'));
		if ($model->where(array('id' => array('NEQ',$data['id']),'mobile' => $data['mobile']))->find()) $this->ajaxReturn(array('code' => 0,'msg' => '手机号码已存在'));
		if (!empty($data['password'])){
			$data['passsalt'] = create_salt();
			$data['password'] = optimizedSaltPwd($data['password'], $data['passsalt']);
		}else{
			unset($data['password']);
		}
		if($model->update($data)){
			db('auth_group_access')->where(array('uid' => $data['id']))->delete();
			db('auth_group_access')->insert(array('uid' => $data['id'],'group_id' => $data['group_id']));
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'url' => url('Manager/index'),'msg' => '编辑成功'));
		} else{
			$this->error('编辑失败');
		}
	}
	
	public function group(){
		$lists = db('auth_group')->paginate(config('PAGE_SIZE'));
		$this->assign('page',$lists->render());
		$this->assign('lists',$lists);
		return $this->fetch();
	}
	
	public function deletegroup(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		if (db('auth_group')->where(array('id' => $id))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
		}
	}
	
	public function add_group(){
		return $this->fetch();
	}
	
	public function editgroup(){
		$id = intval(input('id'));
		$data = db('auth_group')->where(array('id' => $id))->find();
		if (empty($data)) $this->error('数据不存在');
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	public function addgroup(){
		if (!request()->isAjax()) $this->error('操作失败');
		$name = input('post.name');
		if (empty($name)) $this->error('组名称不能为空');
		$data = array(
			'title' => $name,
			'status' => input('post.status'),
			'time' => time(),
		);
		$model = db('auth_group');
		if ($model->where(array('title' => $name))->find()) $this->error('组名称已存在');
		if($model->insert($data)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'url' => url('Manager/group'),'msg' => '新增成功'));
		} else{
			$this->error('新增失败');
		}
	}
	
	public function runedit_group(){
		if (!request()->isAjax()) $this->error('操作失败');
		$name = input('post.name');
		$id = input('post.id');
		if (!$id) $this->error('提交失败');
		if (empty($name)) $this->error('组名称不能为空');
		$data = array(
			'title' => $name,
			'status' => input('post.status'),
		);
		$model = db('auth_group');
		if ($model->where(array('title' => $name,'id' => array('NEQ',$id)))->find()) $this->error('组名称已存在');
		if ($model->where(array('id' => $id))->update($data)) {
			$this->ajaxReturn(array('code' => 1,'msg' => '编辑成功'));
		} else{
			$this->ajaxReturn(array('code' => 0,'msg' => '编辑失败'));
		}
	}
	
	public function groupstatus(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = intval(input('id'));
		$model = db('auth_group');
		$data = $model->where(array('id' => $id))->find();
		if (!$data) $this->ajaxReturn(array('code' => 0,'msg' => '操作失败'));
		$status = $data['status'] ? 0 : 1;
		$affected_row = $model->where(array('id' => $data['id']))->setField('status',$status);
		if (!$affected_row) $this->ajaxReturn(array('code' => 0,'msg' => '操作失败'));
		if ($status){
			$this->ajaxReturn(array('classText' => 'btn-success','status' => $status,'code' => 1,'msg' => '开启'));
		}else{
			$this->ajaxReturn(array('classText' => 'btn-warning','status' => $status,'code' => 1,'msg' => '禁用'));
		}
	}
	
	public function admin_status(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = intval(input('id'));
		$model = db('admin');
		$data = $model->where(array('id' => $id))->find();
		if (!$data) $this->ajaxReturn(array('code' => 0,'msg' => '操作失败'));
		$status = $data['status'] ? 0 : 1;
		$affected_row = $model->where(array('id' => $data['id']))->setField('status',$status);
		if (!$affected_row) $this->ajaxReturn(array('code' => 0,'msg' => '操作失败'));
		if ($status){
			$this->ajaxReturn(array('classText' => 'btn-success','status' => $status,'code' => 1,'msg' => '开启'));
		}else{
			$this->ajaxReturn(array('classText' => 'btn-warning','status' => $status,'code' => 1,'msg' => '禁用'));
		}
	}
	
	public function setting(){
		$id = intval(input('id'));
		$data = db('auth_group')->where(array('id' => $id))->find();
		$lists = db('auth_rule')->select();
		$select = getChild($lists,0);
		$this->assign('lists', $select);
		$data['rule_pids'] = explode(',', $data['rule_pids']);
		$data['rules'] = explode(',', $data['rules']);
		$this->assign('data',$data);
		return $this->fetch('setting_rule');
	}
	
	public function rule_bind_runedit(){
		if (!request()->isAjax()) $this->error('操作失败');
		$rule_ipds = implode(',', input('post.parent/a'));
		$rules = implode(',', input('post.rule/a'));
		$data = array(
			'id' => intval(input('post.group_id')),
			'rule_pids' => $rule_ipds,
			'rules' => $rules
		);
		if (db('auth_group')->update($data)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'url' => url('group'),'msg' => '绑定成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '绑定失败'));
		}
	}
	
	//节点管理
	public function node(){
		$lists = db('auth_rule')->select();
		$select = getChild($lists,0);
		$this->assign('select', $select);
		$this->assign('lists',$lists);
		return $this->fetch();
	}

	public function rule_add_runadd(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = $this->rule_post_check();
		if (db('auth_rule')->where(array('name' => $data['name']))->find()) $this->error('节点名称已存在');
		if (db('auth_rule')->insert($data)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '添加成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '添加失败'));
		}
	}
	
	private function rule_post_check(){
		$data = array(
			'parentid' => input('parentid',0),
			'level' => 1,
			'name' => strtolower(input('name')),
			'title' => input('title'),
			'status' => input('status'),
			'css' => input('css'),
			'sort' => input('sort',50),
			'ismenu' => input('ismenu'),
			'condition' => input('condition'),
		);
		if ($data['parentid']){
			$parent = db('auth_rule')->where(array('id' => $data['parentid']))->find();
			$data['level'] = $parent['level']+1;
		}
		if (empty($data['title'])) $this->ajaxReturn(array('code' => 0,'msg' => '权限标题不能为空'));
		if (empty($data['name'])) $this->ajaxReturn(array('code' => 0,'msg' => '权限名称不能为空'));
		preg_match('/\./', $data['name'],$arr);
		if (!empty($arr)) $this->ajaxReturn(array('code' => 0,'msg' => '权限名称不合法'));
		if ($data['parentid']){
			@list($modules,$controller,$action,$ext) = explode('/', $data['name']);
			if (!isset($modules) || !isset($controller) || !isset($action)) $this->ajaxReturn(array('code' => 0,'msg' => '权限名称不合法'));
		}else{
			@list($modules,$controller,$ext) = explode('/', $data['name']);
			if (!isset($modules) || !isset($controller)) $this->ajaxReturn(array('code' => 0,'msg' => '权限名称不合法'));
		}
		return $data;
	}
	
	public function node_status(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = intval(input('id'));
		$model = db('auth_rule');
		$data = $model->where(array('id' => $id))->find();
		if (!$data) $this->ajaxReturn(array('code' => 0,'msg' => '操作失败'));
		$status = $data['status'] ? 0 : 1;
		$affected_row = $model->where(array('id' => $data['id']))->setField('status',$status);
		if (!$affected_row) $this->ajaxReturn(array('code' => 0,'msg' => '操作失败'));
		if ($status){
			$this->ajaxReturn(array('classText' => 'btn-success','status' => $status,'code' => 1,'msg' => '开启'));
		}else{
			$this->ajaxReturn(array('classText' => 'btn-warning','status' => $status,'code' => 1,'msg' => '禁用'));
		}
	}
	
	public function edit_node(){
		$id = intval(input('id'));
		if (!$id) $this->error('操作失败');
		$data = db('auth_rule')->where(array('id' => $id))->find();
		$lists = db('auth_rule')->select();
		$select = getChild($lists,0);
		$this->assign('select', $select);
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	public function rule_edit_runedit(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = $this->rule_post_check();
		$data['id'] = intval(input('id'));
		if (!$data['id']) $this->ajaxReturn(array('code' => 0,'msg' => '编辑失败'));
		if (db('auth_rule')->where(array('name' => $data['name'],'id' => array("NEQ",$data['id'])))->find()) $this->error('节点名称已存在');
		if (db('auth_rule')->update($data)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '编辑失败'));
		}
	}
	
	public function nodesort(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data['id'] = intval(input('id'));
		$data['sort'] = intval(input('sort'));
		if ($data['sort'] < 0) $data['sort'] = 0;
		if (db('auth_rule')->update($data)){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '编辑失败'));
		}
	}
	
	public function deletenode(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		$groupModel = db('auth_group');
		$group_auth = $groupModel->where(array('rules' => array('NEQ','')))->select();
		foreach ($group_auth as $key => $value){
			$rules_array = explode(',', $value['rules']);
			$pids_array = explode(',', $value['rule_pids']);
			foreach ($rules_array as $rk => $rv){
				if ($rv == $id){
					unset($rules_array[$rk]);
				}
			}
			foreach ($pids_array as $pk => $pv){
				if ($pv == $id){
					unset($pids_array[$pk]);
				}
			}
			$update = array(
				'rules' => implode(',', $rules_array),
				'rule_pids' => implode(',', $pids_array)
			);
			$groupModel->where(array('id' => $value['id']))->update($update);
		}
		if (db('auth_rule')->where(array('id' => $id))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
		}
	}
	
}