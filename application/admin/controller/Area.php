<?php
namespace app\admin\controller;

use think\Db;

class Area extends Common {
	
	public function index(){
		$page = Db::name('address_section')->paginate(config('page_size'));
		$lists = Db::table(config('database')['prefix'].'address_section')
		->alias('a')
		->join('__REGION__ b','a.province = b.id','left')
		->join('__REGION__ c','a.city = c.id','left')
		->join('__REGION__ d','a.area = d.id','left')
		->field("a.*,b.name AS province_name,c.name AS city_name,d.name AS area_name")
		->page($page->currentPage(),$page->listRows())->select();
		$this->assign('page',$page->render());
		$this->assign('lists',$lists);
		return $this->fetch();
	}
	
	public function add_section(){
		$province = db('region')->where(array('pid' => 1))->select();
		$this->assign('province',$province);
		return $this->fetch();
	}
	
	public function edit_section(){
		$address_id = intval(input('id'));
		if (!$address_id) $this->error('操作失败');
		$data = db('address_section')->where(array('id' => $address_id))->find();
		$model = db('region');
		$province = $model->where(array('pid' => 1))->select();
		$this->assign('province',$province);
		$city = $model->where(array('pid' => $data['province']))->select();
		$this->assign('city',$city);
		$area = $model->where(array('pid' => $data['city']))->select();
		$this->assign('area',$area);
		$this->assign('data',$data);
		return $this->fetch();
	}
	
	public function add_section_handler(){
		$data = $this->check_post();
		$data['time'] = time();
		$model = db('address_section');
		if ($model->where(array('section' => $data['section']))->find()) $this->error('配送区域名称已存在');
		if($model->insert($data)){
			$this->ajaxReturn(array('code' => 1,'msg' => '新增成功'));
		} else{
			$this->error('新增失败');
		}
	}
	
	private function check_post(){
		if (!request()->isAjax()) $this->error('操作失败');
		$province = input('post.province');
		$city= input('post.city');
		$area= input('post.area');
		$section= input('post.section');
		if (empty($province)) $this->error('请选择一个省份');
		if (empty($city)) $this->error('请选择一个城市');
		// if (empty($area)) $this->error('请选择一个区域');
		if (empty($section)) $this->error('配送区域名称不能为空');
		$data = array(
			'province' => $province,
			'city' => $city,'area' => $area,
			'section' => $section,
			'status' => input('post.status'),
		);
		return $data;
	}
	
	public function edit_section_handler(){
		$data = $this->check_post();
		$data['id'] = input('post.id');
		$model = db('address_section');
		if ($model->where(array('id' => array('NEQ',$data['id']),'section' => $data['section']))->find()) $this->error('配送区域名称已存在');
		if($model->update($data)){
			$this->ajaxReturn(array('code' => 1,'msg' => '编辑成功'));
		} else{
			$this->error('编辑失败');
		}
	}

	public function area_status(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = intval(input('id'));
		$model = db('address_section');
		$data = $model->where(array('id' => $id))->find();
		if (!$data) $this->ajaxReturn(array('code' => 0,'msg' => '操作失败'));
		$status = $data['status'] ? 0 : 1;
		$affected_row = $model->where(array('id' => $data['id']))->setField('status',$status);
		if (!$affected_row) $this->ajaxReturn(array('code' => 0,'msg' => '操作失败'));
		if ($status){
			$this->ajaxReturn(array('classText' => 'btn-success','status' => $status,'code' => 1,'msg' => '开启'));
		}else{
			$this->ajaxReturn(array('classText' => 'btn-warning','status' => $status,'code' => 1,'msg' => '禁用'));
		}
	}
	
	public function delete(){
		if (!request()->isAjax()) $this->error('操作失败');
		$id = input('id');
		if (db('address_section')->where(array('id' => $id))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
		}
	}
	
	public function deletelists(){
		if (!request()->isAjax()) $this->error('操作失败');
		$ids = input('post.ids');
		$ids = explode('|', rtrim($ids,'|'));
		if (db('address_section')->where(array('id' => array('IN',$ids)))->delete()){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '删除成功'));
		}else{
			$this->ajaxReturn(array('code' => 0,'msg' => '删除失败'));
		}
	}
	
}