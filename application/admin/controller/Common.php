<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;
use think\Hook;

class Common extends Controller {
	
	protected $userinfo = '';
	
	protected function _initialize() {
		if (!session('?backend')) $this->redirect('Login/index');
		$admin = session('backend');
		$admin_id = _encrypt($admin['id'],'DECODE');
		session('handler_id',$admin_id);
		$login_info = _encrypt($admin['login_info'],'DECODE');
		if (!$admin_id) $this->redirect('Login/logout');
		$this->userinfo = Db::name('admin')->where(array('id' => $admin_id))->find();
		if (!$this->userinfo) $this->redirect('Login/logout');
		$usershll = optimizedSaltPwd($this->userinfo['email'].$this->userinfo['id'].$this->userinfo['mobile'].$this->userinfo['username'].$this->userinfo['truename'].$_SERVER['HTTP_USER_AGENT'], $this->userinfo['passsalt']);
		if ($login_info != $usershll) $this->redirect('Login/logout');
		$request = Request::instance();
		define('MODULE_NAME', $request->module());
		define('CONTROLLER_NAME', $request->controller());
		define('ACTION_NAME', $request->action());
		define('REQUEST_URL', $request->url());
		$this->menu();
		if (!in_array($this->userinfo['id'], config('AUTH_CONFIG')['NO_AUTH_USER'])){
			$node = MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;
			if (!in_array($node, config('AUTH_CONFIG')['NO_AUTH_URL'])){
				$auth = new \Auth();
				if(!$auth->check($node, $this->userinfo['id'])){
					if ($node == MODULE_NAME.'/index/index'){
						session('backend',null);
						session(null);
					}
					$this->error('您没有权限访问！');
				}
			}
		}
		
		//行为绑定
		/*
		Hook::add('app_init', 'app\admin\behavior\\Test');
		Hook::add('run', 'app\admin\behavior\\Test');
		Hook::add('app_end', 'app\admin\behavior\\Test');
		//监听绑定的行为
		Hook::listen('app_init',$params);
		$params = array(1,2,3,4,5);
		Hook::listen('run',$params);
		Hook::listen('app_end',$params);
		*/
	}
	
	public function get_child_address(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = db('region')->where(array('pid' => input('pid')))->select();
		$html = '';
		// 1省，2市，3区
		$flag = true;
		foreach ($data as $key => $value){
			$html .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		}
		$this->ajaxReturn($html);
	}
	
	protected function ajaxReturn($data){
		exit(json($data)->getContent());
	}
	
	private function menu(){
		$URL = strtolower(MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME);
		$lists = db('auth_rule')->where(array('status' => 1))->order('sort asc')->select();
		$parentid = 0;
		foreach ($lists as $key => $value){
			if ($value['name'] == $URL && $value['parentid']){
				$parent = getParent($lists,$value['id']);
				$parentid = $parent['id'];
				break;
			}
		}
		$top_menu = array();
		if ($parentid){
			foreach ($lists as $key => $value){
				if ($value['parentid'] == $parentid){
					$top_menu[] = array(
						'node' => $value['name'],
						'title' => $value['title'],
						'level' => $value['level'],
						'icon' => $value['css'],
						'id' => $value['id']
					);
				}
			}
		}
		$node = getChild($lists);
		$menu = array();
		foreach ($node as $key => $value){
			if (empty($value['child'])){
				$menu[$value['name']] = array(
					'node' => $value['name'],
					'title' => $value['title'],
					'level' => $value['level'],
					'icon' => $value['css'],
					'id' => $value['id'],
					'subNode' => array(),
				);
			}else{
				$menu[$value['name']] = array(
					'node' => $value['name'],
					'title' => $value['title'],
					'level' => $value['level'],
					'icon' => $value['css'],
					'id' => $value['id'],
					'subNode' => array(),
				);
				$subNode = array();
				foreach ($value['child'] as $childKey => $childValue){
					$subNode[] = array(
						'node' => $childValue['name'],
						'title' => $childValue['title'],
						'level' => $childValue['level'],
						'parentid' => $childValue['parentid']
					);
				}
				$menu[$value['name']]['subNode'] = $subNode;
			}
		}
		$this->assign('top_menu',$top_menu);
		$this->assign('left_URL',$URL);
		$this->assign('left_menu',$menu);
		$this->assign('left_active',$parentid);
	}
	
	//导出csv文件
	protected function put_csv($list,$title,$filename){
		$file_name=$filename.date("Y-m-d",time()).".csv";
		header ( 'Content-Type: application/vnd.ms-excel' );
		header ( 'Content-Disposition: attachment;filename='.$file_name );
		header ( 'Cache-Control: max-age=0' );
		$file = fopen('php://output',"a");
		$limit=1000;
		$calc=0;
		foreach ($title as $v){
			$tit[]=iconv('UTF-8', 'GB2312//IGNORE',$v);
		}
		fputcsv($file,$tit);	//导入表头
		//导入表数据
		foreach ($list as $v){
			$calc++;
			if($limit==$calc){
				ob_flush();
				flush();
				$calc=0;
			}
			foreach ($v as $t){
				$tarr[] = iconv('UTF-8', 'GB2312//IGNORE',$t);
			}
			fputcsv($file,$tarr);
			unset($tarr);
		}
		unset($list);
		fclose($file);
		exit();
	}

}