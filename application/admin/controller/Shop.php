<?php
namespace app\admin\controller;

use think\Db;

class Shop extends Common {
	
	private $db = null;
	private $config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $shopField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
		$this->shopField = array('shopId','shopSn','userId','areaId1','areaId2','areaId3','latitude','longitude','createTime','shopAddress','shopTel','shopName','shopCompany');
	}
	
	public function index(){
		$params = array();
		if (!empty($_REQUEST)){
			$params = $_REQUEST;
			unset($params['page']);
		}
		$map = array();
		$shopSn = input('post.shopSn');
		$shopName = trim(input('post.shopName'));
		$shopTel = input('post.shopTel');
		if ($shopSn != '') $map['shopSn'] = $shopSn;
		if ($shopName != '') $map['shopName'] = array('LIKE',"%{$shopName}%");
		if ($shopTel != '') $map['shopTel'] = $shopTel;
		$lists = $this->db->name('shops')->where($map)->field($this->shopField)
		->order('createTime desc')->paginate(config('page_size'),false,array('query' => $params));
		$this->assign('page',$lists->render());
		$this->assign('lists',$lists);
		$this->assign('db',$this->db);
		$this->assign('shopSn',$shopSn);
		$this->assign('shopTel',$shopTel);
		$this->assign('shopName',$shopName);
		return $this->fetch();
	}
	
	public function setting(){
		$regionModel = db('region');
		$province = $regionModel->where(array('pid' => 1))->select();
		$this->assign('province',$province);
		$this->assign('not_layout',true);
		$shopid = $this->request->param('shopid');
		$this->assign('shopid',$shopid);
		$set = db('set_section')->where(array('shopId' => $shopid))->find();
		$city = '';
		$area = '';
		if (!empty($set)){
			list($province_id,$city_id,$area_id) = explode(',', $set['area_ids']);
			$this->assign('province_id',$province_id);
			$this->assign('city_id',$city_id);
			$this->assign('area_id',$area_id);
			$city = $regionModel->where(array('pid' => $province_id))->select();
			$area = $regionModel->where(array('pid' => $city_id))->select();
			$address_section = db('address_section')->where(array('id' => array('in',$set['section'])))->select();
			$this->assign('address_section',$address_section);
		}
		$this->assign('set',$set ? $set : '');
		$this->assign('city',$city ? $city : '');
		$this->assign('area',$area ? $area : '');
		return $this->fetch();
	}
	
	public function get_area(){
		if (!request()->isAjax()) $this->error('操作失败');
		$city_id= intval(input('city_id'));
		
		$data = db('address_section')->where(array('city' => $city_id,'status' => 1))->select();
		$html = '';
		foreach ($data as $key => $value){
			$html .= '<label><input name="address[]" type="checkbox" value="'.$value['id'].'" class="ace">';
			$html .= '<span class="lbl"> '.$value['section'].'</span></label>';
		}
		$this->ajaxReturn($html);
	}
	
	public function setting_section_handler(){
		if (!request()->isAjax()) $this->error('操作失败');
		$province = $this->request->post('province');
		$city = $this->request->post('city');
		$area = $this->request->post('area');
		$address = $this->request->post('address/a');
		$status= $this->request->post('status');
		$shopid = intval($this->request->post('shopid'));
		if (!$shopid) $this->ajaxReturn(array('code' => 0,'msg' => '设置失败'));
		if (empty($address)) $this->ajaxReturn(array('code' => 0,'msg' => '请选择区域'));
		$location = $this->request->post('location');
		$left = $right = '';
		if (!empty($location)){
			list($left,$right) = explode('|', $location);
		}
		$dataSet = array(
			'shopId' => $shopid,
			'area_ids' => $province.','.$city.','.$area,
			'section' => implode(',', $address),
			'location_left' => $left,
			'location_right' => $right,
			'status' => $status,
			'time' => time());
		Db::name('set_section')->where(array('shopid' => $shopid))->delete();
		Db::name('set_section')->insert($dataSet);
		$this->ajaxReturn(array('code' => 1,'msg' => '设置成功'));
	}
	
}