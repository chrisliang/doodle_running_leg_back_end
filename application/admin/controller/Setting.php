<?php
namespace app\admin\controller;

class Setting extends Common {
	
	public function index(){
		
		$config = db('system')->where(array('name' => 'delivery_config'))->find();
		if (!empty($config)){
			$config['value'] = unserialize($config['value']);
			# add by holen on 2017.12.25
			foreach ($config['value'] as $k => $v) {
				unset ($config['value'][$k]['go_money2']);
				unset ($config['value'][$k]['go_money3']);
				unset ($config['value'][$k]['go_shop_money2']);
				unset ($config['value'][$k]['go_shop_money3']);
			}
			# add end
			$this->assign('config',$config['value']);
		}
		$config_title = array(
			'go_distance' => '平台支持配送距离',
			'go_money' => array('title' => '收用户配送费','desc' => ''),
			// 'go_money2' => array('title' => '收用户配送费','desc' => '小汽车'),
			// 'go_money3' => array('title' => '收用户配送费','desc' => '火车'),
			'go_oto_money' => '给O2O平台配送费',
			'go_shop_money' => array('title' => '给配送员配送费用','desc' => ''),
			// 'go_shop_money2' => array('title' => '给配送员配送费用','desc' => '小汽车'),
			// 'go_shop_money3' => array('title' => '给配送员配送费用','desc' => '火车'),
			'go_distance_time' => '配送到达时间',
			'go_distance_time2' => '预计到达时间',
		);
		$config_desc = array(
			'go_distance' => '设置不同阶段的配送费，配送时间，预计到达时间，假如说填3，就是在0-3(0&nbsp;<&nbsp;x&nbsp;<=3)公里阶段范围',
		);
		$this->assign('confit_title',$config_title);
		$this->assign('config_desc',$config_desc);
		return $this->fetch();
	}
	
	public function edit_setting_run(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = request()->post();
 		$model = db('system');
		$addArray = array();
		foreach ($data['go_distance'] as $key => $value){
			$addArray[$key] = array(
				'go_distance' => $value,
				'go_money' => $data['go_money'][$key],
				// 'go_money2' => $data['go_money2'][$key],
				// 'go_money3' => $data['go_money3'][$key],
				//'go_oto_money' => $data['go_oto_money'][$key],#原本就注释了
				'go_shop_money' => $data['go_shop_money'][$key],
				// 'go_shop_money2' => $data['go_shop_money2'][$key],
				// 'go_shop_money3' => $data['go_shop_money3'][$key],
				'go_distance_time' => $data['go_distance_time'][$key],
				'go_distance_time2' => $data['go_distance_time2'][$key],
			);
			
		}
		$addArray = serialize($addArray);
		if ($model->where(array('name' => 'delivery_config'))->find()){
			$model->where(array('name' => 'delivery_config'))->setField('value',$addArray);
		}else{
			$model->insert(array(
					'name' => 'delivery_config',
					'title' => '配送费设置(商家)',
					'value' => $addArray
			));
		}
		
		$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
	}
	
	public function urgent(){
		$this->assign('user_urgent',db('system')->where(array('name' => 'user_urgent'))->find());
		return $this->fetch();
	}
	
	public function urgentRun(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = request()->post();
		$model = db('system');
		if ($model->where(array('name' => 'user_urgent'))->find()){
			$model->where(array('name' => 'user_urgent'))->setField('value',$data['user_urgent']);
		}else{
			$model->insert(array(
				'name' => 'user_urgent',
				'title' => '加急配送设置',
				'value' => $data['user_urgent']
			));
		}
		
		$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
	}
	
	/**
	 * [user 配送管理--配送设置用户方向]
	 * @return [type] [description]
	 */
	public function user(){		
		$config = db('system')->where(array('name' => 'delivery_user_config'))->find();
		if (!empty($config)){
			$config['value'] = unserialize($config['value']);

			# add by holen on 2017.12.25
			foreach ($config['value'] as $k => $v) {
				unset ($config['value'][$k]['go_money2']);
				unset ($config['value'][$k]['go_money3']);
				unset ($config['value'][$k]['go_shop_money2']);
				unset ($config['value'][$k]['go_shop_money3']);
			}
			# add end
			// dump($config['value']);
			$this->assign('config',$config['value']);
		}
		$config_title = array(
				'go_distance' => '平台支持配送距离',
				'go_money' => array('title' => '收用户配送费','desc' => ''),
				// 'go_money2' => array('title' => '收用户配送费','desc' => '小汽车'),
				// 'go_money3' => array('title' => '收用户配送费','desc' => '火车'),
				'go_oto_money' => '给O2O平台配送费',
				'go_shop_money' => array('title' => '给配送员配送费用','desc' => ''),
				// 'go_shop_money2' => array('title' => '给配送员配送费用','desc' => '小汽车'),
				// 'go_shop_money3' => array('title' => '给配送员配送费用','desc' => '火车'),
				'go_distance_time' => '配送到达时间',
				'go_distance_time2' => '预计到达时间',
		);
		$config_desc = array(
				'go_distance' => '设置不同阶段的配送费，配送时间，预计到达时间，假如说填3，就是在0-3(0&nbsp;<&nbsp;x&nbsp;<=3)公里阶段范围',
		);
		$this->assign('confit_title',$config_title);
		$this->assign('config_desc',$config_desc);
		return $this->fetch();
	}
	
	/**
	 * [editUserRun 添加新的距离]
	 * @return [type] [description]
	 */
	public function editUserRun(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = request()->post();
		$model = db('system');
		$addArray = array();
		foreach ($data['go_distance'] as $key => $value){
			$addArray[$key] = array(
					'go_distance' => $value,
					'go_money' => $data['go_money'][$key],
					// 'go_money2' => $data['go_money2'][$key],
					// 'go_money3' => $data['go_money3'][$key],
					//'go_oto_money' => $data['go_oto_money'][$key],
					'go_shop_money' => $data['go_shop_money'][$key],
					// 'go_shop_money2' => $data['go_shop_money2'][$key],
					// 'go_shop_money3' => $data['go_shop_money3'][$key],
					'go_distance_time' => $data['go_distance_time'][$key],
					'go_distance_time2' => $data['go_distance_time2'][$key],
			);
			
		}
		$addArray = serialize($addArray);
		if ($model->where(array('name' => 'delivery_user_config'))->find()){
			$model->where(array('name' => 'delivery_user_config'))->setField('value',$addArray);
		}else{
			$model->insert(array(
					'name' => 'delivery_user_config',
					'title' => '配送费设置(用户发布)',
					'value' => $addArray
			));
		}
		
		$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
	}
	
	public function agent(){
		$this->assign('user_shop',db('system')->where(array('name' => 'agent_shop_bili'))->find());
		$this->assign('user_delivery',db('system')->where(array('name' => 'agent_delivery_bili'))->find());
		return $this->fetch();
	}
	
	public function agentRun(){
		if (!request()->isAjax()) $this->error('操作失败');
		$data = request()->post();
		$model = db('system');
		$model->where(array('name' => 'agent_shop_bili'))->delete();
		$model->where(array('name' => 'agent_delivery_bili'))->delete();
		if ($model->insertAll(array(
				['name' => 'agent_shop_bili','title' => '店铺代理提成点','value' => trim($data['agent_shop_bili'])],
				['name' => 'agent_delivery_bili','title' => '跑腿代理提成点','value' => trim($data['agent_delivery_bili'])]))){
			$this->ajaxReturn(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
		}
		$this->ajaxReturn(array('code' => 0,'reload' => 1,'msg' => '编辑失败'));
	}

	/**
	 * [refund 退单设置]
	 * @return [mixed] [description]
	 */
	public function refund(){
		$config = db('system')->where(array('name' => 'user_refund_set'))->find();
		$this->assign('config',$config);
		return $this->fetch();
	}

	/**
	 * [refundRun 修改退单设置]
	 * @return [type] [description]
	 */
	public function refundRun(){
		// if (!request()->isAjax()) return json(array('code'=>0,'reload'=>1,'msg'=>'操作失败'));
		if($this->request->isAjax()){
			$value = input('value');
			$data = [];
			$data['value'] = $value;
			if(db('system')->where(['name'=>'user_refund_set'])->update($data)){
				return json(array('code' => 1,'reload' => 1,'msg' => '编辑成功'));
			}else{
				return json(array('code' => 0,'reload' => 1,'msg' => '编辑失败'));
			}
		}
	}
	
}