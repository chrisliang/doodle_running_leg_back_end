{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					添加优惠券
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" name="add" method="post" action="<?php echo url('run_add');?>">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 优惠券标题：  </label>
						<div class="col-sm-10">
							<input type="text" name="title" id="title" placeholder="输入优惠券标题" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 减免额度：  </label>
						<div class="col-sm-10">
							<input type="text" name="money" id="money" placeholder="输入减免额度" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 失效时间：  </label>
						<div class="col-sm-10">
							<input type="text" name="endtime" value="" class="col-xs-10 col-sm-4 layui-input" required="required" id="test1">
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 发放类型： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<label for="type">
							<input name="type" id="type" value="1" checked="checked" class="ace ace-switch ace-switch-4 btn-flat" type="radio" />
							<span class="lbl">&nbsp;全部用户</span></label>
							&nbsp; &nbsp; &nbsp;
							<label for="type2">
							<input name="type" id="type2" value="2" class="ace ace-switch ace-switch-4 btn-flat" type="radio" />
							<span class="lbl">&nbsp;部份用户</span>
							</label>
						</div>
						
					</div>
					<div class="space-4"></div>
								<div class="row userlist" style="display:none;">
									<div class="col-xs-12">
										

										<!-- <div class="table-responsive"> -->

										<!-- <div class="dataTables_borderWrap"> -->
										<div>
											<table id="sample-table-2" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th class="center">
															<label class="position-relative">
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>
														</th>
														<th>UID</th>
														<th>用户名</th>
														<th>手机号</th>

													</tr>
												</thead>

												<tbody>
												<?php foreach ($users as $key => $value){ ?>
													<tr>
														<td class="center">
															<label class="position-relative">
																<input type="checkbox" name="uid[]" value="<?php echo $value['userId'];?>" class="ace" />
																<span class="lbl"></span>
															</label>
														</td>

														<td>
															<a href="###"><?php echo $value['userId'];?></a>
														</td>
														<td><?php echo $value['userName'];?></td>
														<td><?php echo $value['userPhone'];?></td>
														
													</tr>
<?php }?>

												</tbody>
											</table>
										</div>
									</div>
								</div>

					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								发送
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script src="__PUBLIC__/assets/js/jquery.dataTables.js"></script>
<script src="__PUBLIC__/assets/js/jquery.dataTables.bootstrap.js"></script>
<script src="__PUBLIC__/layui/layui.js"></script>
<script src="__PUBLIC__/laydate/laydate.js"></script>
<link rel="stylesheet" href="__PUBLIC__/laydate/theme/default/laydate.css" />
<script>
$(function(){
	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#test1',
		    type:'datetime' //指定元素
		  });
	});
	$('#type').click(function(){
		$('.userlist').hide();
	});
	$('#type2').click(function(){
		$('.userlist').show();
	});
	var oTable1 = $('#sample-table-2')
	//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
	.dataTable( {
		bAutoWidth: false,
		"aoColumns": [{ "bSortable": false },null,null,{ "bSortable": false }],
		"aaSorting": [],
		"oLanguage": {
            "sLengthMenu": "每页显示 _MENU_  条",  
            "sZeroRecords": "没有找到符合条件的数据",  
            "sProcessing": "我也是醉了，正在加载数据...",  
            "sInfo": "当前第 _START_ - _END_ 条　共计 _TOTAL_ 条",  
            "sInfoEmpty": "没有有记录",  
            "sInfoFiltered": "(从 _MAX_ 条记录中过滤)",  
            "sSearch": "搜索：",  
            "oPaginate": {  
                "sFirst": "首页",  
                "sPrevious": "前一页",  
                "sNext": "后一页",  
                "sLast": "尾页"  
            }  
},

    } );
	
});
</script>
{/block}
