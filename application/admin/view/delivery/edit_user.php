{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					编辑配送员
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" name="add_user" method="post" action="<?php echo url('Delivery/edit_user_handler');?>">
					<input type="hidden" name="id" value="{$data.id}"/>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 手机号码：  </label>
						<div class="col-sm-10">
							<input type="text" name="mobile" id="mobile" placeholder="输入手机号码" value="<?php echo $data['mobile'];?>" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 登录密码：  </label>
						<div class="col-sm-10">
							<input type="password" name="password" id="password" placeholder="输入登录密码" class="col-xs-10 col-sm-4"/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 提现密码：  </label>
						<div class="col-sm-10">
							<input type="password" name="det_password" id="det_password" placeholder="输入提现密码" class="col-xs-10 col-sm-4"/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 用户名：  </label>
						<div class="col-sm-10">
							<input type="text" name="username" id="username" placeholder="输入用户名" value="<?php echo $data['username'];?>" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 真实姓名：  </label>
						<div class="col-sm-10">
							<input type="text" name="truename" id="truename" placeholder="输入真实姓名" value="<?php echo $data['truename'];?>" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 身份证号：  </label>
						<div class="col-sm-10">
							<input type="text" name="id_card" id="id_card" placeholder="输入身份证号" value="<?php echo $data['id_card'];?>" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 是否启用： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="status" id="status" value="1" {if condition="$data['status']"}checked="checked"{/if} class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 支持配送地区： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<select name="province" id="province_id" required="required">
							<option value="0">请选择省份</option>
							{foreach name="province" item="v"}
								<option value="{$v.id}" {if condition="$v['id'] eq $data['province']"}selected="selected"{/if}>{$v.name}</option>
							{/foreach}
							</select>
							<select name="city" id="city_id" required="required">
							<option value="0">请选择城市</option>
							{foreach name="city" item="v"}
								<option value="{$v.id}" {if condition="$v['id'] eq $data['city']"}selected="selected"{/if}>{$v.name}</option>
							{/foreach}
							</select>
							<select name="area" id="area_id" style="display: none;">
							<option value="0">请选择区域</option>
							{foreach name="area" item="v"}
								<option value="{$v.id}" {if condition="$v['id'] eq $data['area']"}selected="selected"{/if}>{$v.name}</option>
							{/foreach}
							</select>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div style="display: none;" class="form-group" id="checkbox_address_group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 勾选配送区域：  </label>
						<div class="col-sm-10">
							<div id="checkbox_address" class="checkbox">{$checkbox_html}</div>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 头像上传： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_face" value="<?php echo $data['user_face'];?>"/>
								<input type="file" name="user_face" id="file0" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="/{$data.user_face}" width="100" height="70" id="img0" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;上传前先用PS处理成等比例图片后上传，最后都统一比例 ：100px*100px<br />
						</span>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 上传手持身份证照片： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
							<input type="hidden" name="org_card_thumb" value="<?php echo $data['card_thumb'];?>"/>
								<input type="file" name="card_thumb" id="file1" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="/{$data.card_thumb}" width="100" height="70" id="img1" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic1('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 上传身份证正面照片： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
							<input type="hidden" name="org_card_thumb2" value="<?php echo $data['card_thumb2'];?>"/>
								<input type="file" name="card_thumb2" id="file2" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="/{$data.card_thumb2}" width="100" height="70" id="img2" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic2('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 上传身份证背面照片： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
							<input type="hidden" name="org_card_thumb3" value="<?php echo $data['card_thumb3'];?>"/>
								<input type="file" name="card_thumb3" id="file3" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="/{$data.card_thumb3}" width="100" height="70" id="img3" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic3('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('#province_id').change(function(){
		$('#checkbox_address').html('');
		$('#checkbox_address_group').hide();
		var province_id  = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo url('Common/get_child_address');?>',
			dataType:'json',
			data : {pid:province_id},
			success:function(data){
				var data = '<option value="0">请选择城市</option>'+data;
				$('#city_id').html(data);
				var data = '<option value="0">请选择区域</option>';
				$('#area_id').html(data);
			},
		});
	});
// 	$('#city_id').change(function(){
//		$('#checkbox_address').html('');
//		$('#checkbox_address_group').hide();
//		var city_id = $(this).val();
//		$.ajax({
//			type:'get',
//			url:'<?php echo url('Common/get_child_address');?>',
//			dataType:'json',
//			data : {pid:city_id},
//			success:function(data){
//				var data = '<option value="0">请选择区域</option>'+data;
//				$('#area_id').html(data);
//			},
//		});
//	});
//	$('#area_id').change(function(){
//		var area_id = $(this).val();
//		$.ajax({
//			type:'get',
//		url:'<?php echo url('Delivery/get_area');?>',
//			dataType:'json',
//			data : {area_id:area_id},
//			success:function(html){
//				$('#checkbox_address').html(html);
//				$('#checkbox_address_group').show();
//			},
//		});
//	});

$('#city_id').change(function(){
	var city_id = $(this).val();
	$.ajax({
		type:'get',
		url:'<?php echo url('Delivery/get_area');?>',
		dataType:'json',
		data : {city_id:city_id},
		success:function(html){
			$('#checkbox_address').html(html);
			$('#checkbox_address_group').show();
		},
	});
});
});
</script>
{/block}