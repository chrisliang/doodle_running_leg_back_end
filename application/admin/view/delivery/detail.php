{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="post" action="">
	<input type="hidden" name="queryType" value="year"/>
		<label class="inline">
			<span class="lbl">订单年走势</span>
		</label>
		<div class="input-group">
			<input type="text" name="startYear" value="{$startYear}" class="layui-input" id="test1">
			<span class="input-group-addon">
				<i class="fa fa-calendar bigger-110"></i>
			</span>
		</div>
		<label class="inline">
			<span class="lbl">-</span>
		</label>
				<div class="input-group">
			<input type="text" name="endYear" value="{$endYear}" class="layui-input" id="test2">
			<span class="input-group-addon">
				<i class="fa fa-calendar bigger-110"></i>
			</span>
		</div>
		
		<button type="submit" name="query" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
		
		<button type="submit" name="export" class="btn btn-info btn-sm">
			<i class="ace-icon fa fa-share"></i>导出
		</button>
	</form>
	
		<form class="form-inline" method="post" action="" style="margin-top:10px;">
		<input type="hidden" name="queryType" value="month"/>
		<label class="inline">
			<span class="lbl">订单月走势</span>
		</label>
		<div class="input-group">
			<input type="text" name="startMonth" value="{$startMonth}" class="layui-input" id="test3">
			<span class="input-group-addon">
				<i class="fa fa-calendar bigger-110"></i>
			</span>
		</div>
		<label class="inline">
			<span class="lbl">-</span>
		</label>
				<div class="input-group">
			<input type="text" name="endMonth" value="{$endMonth}" class="layui-input" id="test4">
			<span class="input-group-addon">
				<i class="fa fa-calendar bigger-110"></i>
			</span>
		</div>
		
		<button type="submit" name="query" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
		
		<button type="submit" name="export" class="btn btn-info btn-sm">
			<i class="ace-icon fa fa-share"></i>导出
		</button>
	</form>
	</div>
</div>


   <ul id="myTab" class="nav nav-tabs wst-tab" role="tablist">
      <li class="active"><a href="#tabc0" role="tab" data-toggle="tab" data='0'>订单走势</a></li>
   </ul>
   <div class='tab-content wst-tab-content'>
      <div class='tab-pane active in fade wst-tab-pane' id='tabc0'>
         <div id="container0_1" style="height: 400px; width:100%;margin: 0 auto;"></div>
      </div>
 
   </div>

<?php if (isset($page)):?>
<div class="pager">
<?php echo $page;?>
</div>
<?php endif;?>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script src="__JS__/highcharts.js"></script>
<script src="__PUBLIC__/layui/layui.js"></script>
<script src="__PUBLIC__/laydate/laydate.js"></script>
<link rel="stylesheet" href="__PUBLIC__/laydate/theme/default/laydate.css" />
<script type="text/javascript">
var s = '{$series}';
viewChart(JSON.parse(s),0);
function viewChart(data){   
    $('#container0_1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '订单走势'
        },
        xAxis: {
            categories: data.categories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: '数量'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: data.series
    });
  }
$(function(){
	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#test1',
		    type:'year' //指定元素
		  });
		  laydate.render({
			    elem: '#test2',
			    type:'year' //指定元素
		   });
		  laydate.render({
			    elem: '#test3',
			    type:'month' //指定元素
			  });
			  laydate.render({
				    elem: '#test4',
				    type:'month' //指定元素
			   });
	});
});
</script>
{/block}
