{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="post" action="">
		<label class="inline">
			<span class="lbl">日期</span>
		</label>
		<div class="input-group">
			<input type="text" name="start" value="{$startTime}" class="layui-input" id="test1">
			<span class="input-group-addon">
				<i class="fa fa-calendar bigger-110"></i>
			</span>
		</div>
		<label class="inline">
			<span class="lbl">-</span>
		</label>
				<div class="input-group">
			<input type="text" name="end" value="{$endTime}" class="layui-input" id="test2">
			<span class="input-group-addon">
				<i class="fa fa-calendar bigger-110"></i>
			</span>
		</div>
		
		<button type="submit" name="query" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
		<button type="submit" name="export" class="btn btn-info btn-sm">
			<i class="ace-icon fa fa-share"></i>导出
		</button>
	</form>
	</div>
</div>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>配送员数</th>
			<th>有配送完成订单的配送人数</th>
			<th>配送完成总订单数</th>
			<th>退单数</th>
		</tr>
	</thead>

	<tbody>
		
		<tr id="tr">
			
			<td>
				{$totalUser}
			</td>
			<td>
				{$finishOrderUser}
			</td>
			<td class="hidden-480">
				{$finishOrder}
			</td>
			<td>{$retreatOrder}</td>
		</tr>
	
	</tbody>
</table>
<?php if (isset($page)):?>
<div class="pager">
<?php echo $page;?>
</div>
<?php endif;?>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script src="__PUBLIC__/layui/layui.js"></script>
<script src="__PUBLIC__/laydate/laydate.js"></script>
<link rel="stylesheet" href="__PUBLIC__/laydate/theme/default/laydate.css" />
<script type="text/javascript">
$(function(){
	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#test1',
		    type:'datetime' //指定元素
		  });
		  laydate.render({
			    elem: '#test2',
			    type:'datetime' //指定元素
		   });
	});
// 	$('.date-picker').datepicker({
// 		autoclose: true,
// 		todayHighlight: true
// 	});
// 	$('#date-timepicker1,#date-timepicker2').datetimepicker().next().on(ace.click_event, function(){
// 		$(this).prev().focus();
// 	});
});
</script>
{/block}
