{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
						<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					配送设置
				</small></h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
				
					{include file="public/top_menu"}


		<section class="content">

            <div class="tabbable">
				<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a data-toggle="tab" href="#home">配送费设置</a>
				</li>
<!-- 				<li>
					<a data-toggle="tab" href="#set">配送员配送时间管理</a>
				</li>
				<li>
					<a data-toggle="tab" href="#set2">预计配送到达时间管理</a>
				</li> -->
				</ul>
				<form class="form-horizontal ajaxForm" name="system_config" method="post" action="<?php echo url('edit_setting_run');?>">
				<div class="tab-content">
				<span class="btn btn-default add-config">添加距离</span>
				
<!-- <div class="col-xs-12 col-sm-12 rule-top alert alert-info top10" style="margin-top:10px;margin-bottom:5px;">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	1、平台支持配送距离： (公里) 这个距离将决定用户能距离商家多远接单(可以填小数）<br>
	2、收用户配送费：(配送费)默认没有，可添加。0.5公里开始起跳，配送费向上取整，如：大于0.5公里不足一公里，配送费按1公里算<br>
	3、不同距离的配送时间：(分钟) 默认没有，可添加。0.5公里开始起跳，配送时间向上取整，如：大于0.5公里不足一公里，配送时间按1公里算<br>
	4、不同距离的预计到达时间：(分钟) 默认没有，可添加。0.5公里开始起跳，预计配送时间向上取整，如：大于0.5公里不足一公里，预计配送时间按1公里算
</div> -->
				<?php if (isset($config)){?>
				{foreach name="config" item="v"}
				<div class="tab-pane fade in active tab-pane-input">
					<?php if ($key>0){?>
					<?php echo '<a href="javascript:;" class="btn btn-default btn-delete" style="margin:10px;">删除</a>';?>
					<?php }?>
					{foreach name="v" item="vv"}
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> <?php if (is_array($confit_title[$key])){echo $confit_title[$key]['title'];}else{echo $confit_title[$key];}?>：  </label>
						<div class="col-sm-10">
							<input type="text" name="{$key}[]" value="<?php echo $vv;?>" class="col-xs-2 col-sm-2"/>
							<span class="help-inline col-xs-12 col-sm-5">
							<span class="middle" id="resone"><?php if (isset($config_desc[$key])){
				echo $config_desc[$key];
							} if (is_array($confit_title[$key])){echo $confit_title[$key]['desc'];}?></span>
							</span>
						</div>
					</div>
					<div class="space-4"></div>
					{/foreach}
					<?php if ($key>0){?>
					<div style="border-top:1px dotted #333;padding-top:15px;"></div>
					<?php }?>
					</div>
					{/foreach}
					<?php }?>
						<div class="clearfix appendHmtl">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
					
					
				</div>
				</form>
			</div>

        </section>
					
					
					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<template>
<div class="tab-pane fade in active tab-pane-input" style="border-top:1px dotted #333;">
	<a href="javascript:;" class="btn btn-default btn-delete" style="margin:10px;">删除</a>
	<div class="form-group" style="padding-top: 15px;">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 平台支持配送距离：  </label>
		<div class="col-sm-10">
			<input type="text" name="go_distance[]" placeholder="输入平台支持配送距离" value="" class="col-xs-2 col-sm-2"/>
			<span class="help-inline col-xs-12 col-sm-5">
			<span class="middle" id="resone">设置不同阶段的配送费，配送时间，预计到达时间，假如说填3，就是在0-3(0&nbsp;<&nbsp;x&nbsp;<=3)公里阶段范围</span>
			</span>
		</div>
	</div>
	<div class="space-4"></div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 收用户配送费：  </label>
		<div class="col-sm-10">
			<input type="text" name="go_money[]" id="" placeholder="输入配送费用" value="" class="col-xs-2 col-sm-2" required/> 
		</div>
		<div class="space-4" style="margin-top:5px;"></div>
		<!-- <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 收用户配送费：  </label>
		<div class="col-sm-10"  style="margin-top:5px;">
			<input type="text" name="go_money2[]" id="" placeholder="输入配送费用" value="" class="col-xs-2 col-sm-2" required/> 小汽车
		</div>
		<div class="space-4" style="margin-top:5px;"></div>
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 收用户配送费：  </label>
		<div class="col-sm-10"  style="margin-top:5px;">
			<input type="text" name="go_money3[]" id="" placeholder="输入配送费用" value="" class="col-xs-2 col-sm-2" required/> 火车
		</div> -->
	</div>
<!-- 
	<div class="space-4"></div>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 给O2O平台配送费：  </label>
		<div class="col-sm-10">
			<input type="text" name="go_oto_money[]" id="" placeholder="输入配送费用" value="" class="col-xs-2 col-sm-2" required/>
		</div>
	</div>
-->
	<div class="space-4"></div>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 给配送员配送费用：  </label>
		<div class="col-sm-10">
			<input type="text" name="go_shop_money[]" id="" placeholder="输入配送费用" value="" class="col-xs-2 col-sm-2" required/>
		</div>
		<!-- <label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 给配送员配送费用：  </label>
		<div class="col-sm-10" style="margin-top:5px;">
			<input type="text" name="go_shop_money2[]" id="" placeholder="输入配送费用" value="" class="col-xs-2 col-sm-2" required/> 小汽车
		</div>
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 给配送员配送费用：  </label>
		<div class="col-sm-10" style="margin-top:5px;">
			<input type="text" name="go_shop_money3[]" id="" placeholder="输入配送费用" value="" class="col-xs-2 col-sm-2" required/> 火车
		</div> -->
	</div>
	<div class="space-4"></div>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 配送到达时间： </label>
		<div class="col-sm-10">
			<input name="go_distance_time[]" id="" placeholder="输入配送到达时间（分钟）" class="col-xs-2 col-sm-2" value="" type="text">
	<span class="help-inline col-xs-12 col-sm-5">
		<span class="middle" id="resone"></span>
	</span>
		</div>
	</div>
	<div class="space-4"></div>
	<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 预计到达时间： </label>
			<div class="col-sm-10">
				<input name="go_distance_time2[]" id="" placeholder="输入预计到达时间（分钟）" class="col-xs-2 col-sm-2" value="" type="text">
		<span class="help-inline col-xs-12 col-sm-5">
			<span class="middle" id="resone"></span>
		</span>
			</div>
		</div>
		<div class="space-4"></div>
	</div>
</template>
<script type="text/javascript">
$(function(){

	$('body').on('click','.btn-delete',function(){
		$(this).parent().remove();
	});
	
	$('.add-config').click(function(){

		$('.appendHmtl').before($('template').html());

	});
	
});
</script>
{/block}