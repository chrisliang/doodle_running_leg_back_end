{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="row maintop">
<div class="col-xs-12 col-sm-2">
<a href="{:url('articlecats/toEdit')}">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增
</button>
</a>
</div>
</div>


<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
           <th>序号</th>
           <th>分类名称</th>
           <th>排序号</th>
           <th>是否显示</th>
           <th>操作</th>
		</tr>
	</thead>

	<tbody>
		<!-- 一级菜单  -->
		{volist name="list" id="vo"}
		<tr>
		   <td>{$i}</td>
           <td>
           		<span class='glyphicon glyphicon-plus' onclick="hideChild(this,{$vo.catId},1)" style='margin-right:3px;cursor:pointer'></span>
           		<input type="text" name="catName" onblur="editCatName({$vo.catId},this)" value="{$vo.catName}" >
           </td>
           <td>{$vo['catSort']}</td>
           <td>
           <div class="dropdown">
           	{if condition="$vo['isShow']==0"}
			    <button type="button" class="btn dropdown-toggle" id="btn_{$vo['catId']}" data-status="{$vo.isShow}" data-toggle="dropdown">隐藏<span class="caret"></span></button>
			{else /}
				<button type="button" class="btn dropdown-toggle" id="btn_{$vo['catId']}" data-toggle="dropdown">显示<span class="caret"></span></button>
			{/if}
			    <ul class="dropdown-menu" role="menu" aria-labelledby="btn_{$vo['catId']}">
				  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(1,{$vo['catId']});">显示</a></li>
				  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(0,{$vo['catId']});">隐藏</a></li>
			    </ul>
			</div>
           </td>
			<td style="width:160px;">		
				<div class="hidden-sm hidden-xs btn-group">
					<button class="btn btn-xs btn-success" onclick="window.location.href='<?php echo url('articlecats/toEdit',array('pid'=>$vo['catId']));?>'">
						<i class="ace-icon fa fa-plus bigger-120"></i>
					</button>
					<button class="btn btn-xs btn-info" onclick="window.location.href='<?php echo url('articlecats/toEdit',array('id' => $vo['catId']));?>'">
						<i class="ace-icon fa fa-pencil bigger-120"></i>
					</button>
					<button class="btn btn-xs btn-danger ajaxDelete" data-id="{$vo.catId}" action="<?php echo url('articlecats/del',array('id' => $vo['catId']));?>">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
					</button>

				</div>
			</td>
		</tr>
	</tbody>

		{volist name="$vo.child" id="v1" key="k"}
		<tbody class="child_{$vo.catId}" style="display:none;">
			<tr >
			   <td  style="padding-left: 30px;">{$k}</td>
	           <td  style="padding-left: 30px;">
	           		<span class='glyphicon glyphicon-plus' onclick="hideChild(this,{$v1.catId},3)" style='margin-right:3px;cursor:pointer'></span>
	           		<input type="text" name="catName" onblur="editCatName({$v1.catId},this)" value="{$v1.catName}" >
	           </td>
	           <td>{$v1['catSort']}</td>
	           <td>
	           <div class="dropdown">
	           	{if condition="$v1['isShow']==0"}
				    <button type="button" class="btn dropdown-toggle" id="btn_{$v1['catId']}" data-status="{$v1.isShow}" data-toggle="dropdown">隐藏<span class="caret"></span></button>
				{else /}
					<button type="button" class="btn dropdown-toggle" id="btn_{$v1['catId']}" data-toggle="dropdown">显示<span class="caret"></span></button>
				{/if}
				    <ul class="dropdown-menu" role="menu" aria-labelledby="btn_{$v1['catId']}">
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(1,{$v1['catId']});">显示</a></li>
					  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(0,{$v1['catId']});">隐藏</a></li>
				    </ul>
				</div>
	           </td>
				<td style="width:160px;">		
					<div class="hidden-sm hidden-xs btn-group">
						<button class="btn btn-xs btn-success" onclick="window.location.href='<?php echo url('articlecats/toEdit',array('pid'=>$v1['catId']));?>'">
							<i class="ace-icon fa fa-plus bigger-120"></i>
						</button>
						<button class="btn btn-xs btn-info" onclick="window.location.href='<?php echo url('articlecats/toEdit',array('id' => $v1['catId']));?>'">
							<i class="ace-icon fa fa-pencil bigger-120"></i>
						</button>
						<button class="btn btn-xs btn-danger ajaxDelete" data-id="{$v1.catId}" action="<?php echo url('articlecats/del',array('id' => $v1['catId']));?>">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
						</button>
					</div>
				</td>
			</tr>
		</tbody>
				<!-- 三级菜单 -->
			{volist name="$v1.child" id="v2" key="k2"}
			<tbody class="child_{$v1.catId}" data-parent="parent_{$vo.catId}" style="display:none;">
				<tr >
				   <td  style="padding-left: 60px;">{$k2}</td>
		           <td  style="padding-left: 60px;">
		           		<input type="text" name="catName" onblur="editCatName({$v2.catId},this)" value="{$v2.catName}" >
		           </td>
		           <td>{$v2['catSort']}</td>
		           <td>
		           <div class="dropdown">
		           	{if condition="$v2['isShow']==0"}
					    <button type="button" class="btn dropdown-toggle" id="btn_{$v2['catId']}" data-status="{$v2.isShow}" data-toggle="dropdown">隐藏<span class="caret"></span></button>
					{else /}
						<button type="button" class="btn dropdown-toggle" id="btn_{$v2['catId']}" data-toggle="dropdown">显示<span class="caret"></span></button>
					{/if}
					    <ul class="dropdown-menu" role="menu" aria-labelledby="btn_{$v2['catId']}">
						  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(1,{$v2['catId']});">显示</a></li>
						  <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:toggleIsShow(0,{$v2['catId']});">隐藏</a></li>
					    </ul>
					</div>
		           </td>
					<td style="width:160px;">		
						<div class="hidden-sm hidden-xs btn-group">
							<button class="btn btn-xs btn-info" onclick="window.location.href='<?php echo url('articlecats/toEdit',array('id' => $v2['catId']));?>'">
								<i class="ace-icon fa fa-pencil bigger-120"></i>
							</button>
							<button class="btn btn-xs btn-danger ajaxDelete" data-id="{$v2.catId}" action="<?php echo url('articlecats/del',array('id' => $v2['catId']));?>">
								<i class="ace-icon fa fa-trash-o bigger-120"></i>
							</button>
						</div>
					</td>
				</tr>
			</tbody>
				{/volist}

			{/volist}

		{/volist}
		

</table>
<div class="pager">

</div>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
	//状态切换
	function toggleIsShow(status,id){
		var message = status?'显示':'隐藏'
		var status = status
		var id = id
		var url = "{:url('admin/articlecats/toggleIsShow')}";
		$.post(url,{id:id,status:status},function(e){
			if(e.status==1){
				$("#btn_"+id).attr('data-status',e.state)
				$("#btn_"+id).html(message+'<span class="caret"></span>');
				layer.msg(e.msg);
			}else{
				layer.msg(e.msg);
				return false;
			}
		},'json')
	}

	//修改分类名称
	function editCatName(id,obj){
		var catName = $(obj).val();
		if(catName == ''){
			layer.msg('不能为空');
			return false
		}
		var id = id
		var url = "{:url('admin/articlecats/editCatName')}";
		$.post(url,{id:id,catName:catName},function(e){
			if(e.status==1){
				$(obj).val(catName)
				layer.msg(e.msg);
			}else{
				layer.msg(e.msg);
				return false;
			}
		},'json')
	}

	//子类的隐藏跟显示
	function hideChild(obj,id,type=0){
		if($(obj).hasClass('glyphicon-plus')){
			$(obj).attr('class','glyphicon glyphicon-minus');
			var len=$(".child_"+id).length;
			console.log(id)
			$(".child_"+id).attr('style',"");
		}else{
			$(obj).attr('class','glyphicon glyphicon-plus');
			$(".child_"+id).attr('style',"display:none;")
			if(type==1){
				console.log("tbody[data-parent='parent_"+id+"']")
				$("tbody[data-parent='parent_"+id+"']").attr('style','display:none;')
			}
		}
	}

</script>
{/block}
