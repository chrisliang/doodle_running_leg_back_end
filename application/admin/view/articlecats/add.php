{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					新增文章分类
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" name="add" method="post" action="<?php echo url('articlecats/add');?>">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 分类名称：  </label>
						<div class="col-sm-10">
							<input type="hidden" value="{$pid}" name="pid">
							<input type="text" name="catName" id="catName" placeholder="分类名称" class="col-xs-10 col-sm-4" maxlength="20" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 是否显示：  </label>
						<div class="col-sm-10">
							<label>
							<input type='radio' id='isShow' name='isShow' checked value='1'/>是
							</label>
							<label>
								<input type='radio' id='isShow' name='isShow' value='0'/>否
							</label>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 排序号：  </label>
						<div class="col-sm-10">
							<input type="text" name="catSort" id="catSort" placeholder="排序号" class="col-xs-10 col-sm-4" />
						</div>
					</div>
					<div class="space-4"></div>
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<a class="btn" href="javascript:window.history.back();">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								返回
							</a>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<link href="__PUBLIC__/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="__PUBLIC__/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="__PUBLIC__/datetimepicker/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript">
$(function(){

});
</script>
{/block}