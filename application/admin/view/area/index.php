{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<div class="row maintop">
<div class="col-xs-12 col-sm-2">
<a href="{:url('Area/add_section')}">
<button class="btn btn-sm btn-danger">
<i class="ace-icon fa fa-bolt bigger-110"></i>新增配送地区
</button>
</a>
</div>
</div>


<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="position-relative">
					<input class="ace" type="checkbox">
					<span class="lbl"></span>
				</label>
			</th>
			<th>ID</th>
			<th>配送区域</th>
			<th class="hidden-480">新增时间</th>
			<th>状态</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($lists as $key => $value) { ?>
		<tr>
			<td class="center">
				<label class="position-relative">
					<input class="ace" value="{$value.id}" name="checkbox[]" type="checkbox">
					<span class="lbl"></span>
				</label>
			</td>
			<td>{$value.id}</td>
			<td>{$value.province_name}-{$value.city_name}-{$value.section}</td>
			<td class="hidden-480">{$value.time|date='Y-m-d',###}</td>
			<td>
				<?php if ($value['status']){?>
				<button class="btn btn-xs btn-success ajaxStatus" action="<?php echo url('Area/area_status');?>" data-id="<?php echo $value['id'];?>">开启</button>
				<?php }else{ ?>
				<button class="btn btn-xs btn-warning ajaxStatus" action="<?php echo url('Area/area_status');?>" data-id="<?php echo $value['id'];?>">禁用</button>
				<?php }?>
			</td>
			<td>
			
				<div class="hidden-sm hidden-xs btn-group">
					<button class="btn btn-xs btn-info" onclick="window.location.href='<?php echo url('Area/edit_section',array('id' => $value['id']));?>'">
						<i class="ace-icon fa fa-pencil bigger-120"></i>
					</button>
	
					<button class="btn btn-xs btn-danger ajaxDelete" data-id="" action="<?php echo url('Area/delete',array('id' => $value['id']));?>">
						<i class="ace-icon fa fa-trash-o bigger-120"></i>
					</button>

				</div>

				<div class="hidden-md hidden-lg">
					<div class="inline position-relative">
						<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
							<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
						</button>

						<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
							
							<li>
								<a href="#" class="tooltip-success" data-rel="tooltip" title="" data-original-title="Edit">
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
									</span>
								</a>
							</li>
					
							<li>
								<a href="#" class="tooltip-error" data-rel="tooltip" title="" data-original-title="Delete">
									<span class="red">
										<i class="ace-icon fa fa-trash-o bigger-120"></i>
									</span>
								</a>
							</li>
			
						</ul>
					</div>
				</div>
				
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
<div class="pager">
<!-- <table cellspacing="0" cellpadding="0" border="0" class="ui-pg-table navtable" style="float:left;table-layout:auto;"><tbody><tr><td class="ui-pg-button ui-corner-all" title="" id="add_grid-table" data-original-title="Add new row"><div class="ui-pg-div"><span class="ui-icon ace-icon fa fa-plus-circle purple"></span></div></td><td class="ui-pg-button ui-corner-all" title="" id="edit_grid-table" data-original-title="Edit selected row"><div class="ui-pg-div"><span class="ui-icon ace-icon fa fa-pencil blue"></span></div></td><td class="ui-pg-button ui-corner-all" title="" id="view_grid-table" data-original-title="View selected row"><div class="ui-pg-div"><span class="ui-icon ace-icon fa fa-search-plus grey"></span></div></td><td class="ui-pg-button ui-corner-all" title="" id="del_grid-table" data-original-title="Delete selected row"><div class="ui-pg-div"><span class="ui-icon ace-icon fa fa-trash-o red"></span></div></td><td class="ui-pg-button ui-state-disabled" style="width:4px;" data-original-title="" title=""><span class="ui-separator"></span></td><td class="ui-pg-button ui-corner-all" title="" id="search_grid-table" data-original-title="Find records"><div class="ui-pg-div"><span class="ui-icon ace-icon fa fa-search orange"></span></div></td><td class="ui-pg-button ui-corner-all" title="" id="refresh_grid-table" data-original-title="Reload Grid"><div class="ui-pg-div"><span class="ui-icon ace-icon fa fa-refresh green"></span></div></td></tr></tbody></table> -->
<table cellspacing="0" cellpadding="0" border="0" class="ui-pg-table navtable" style="float:left;table-layout:auto;">
<tbody>
<tr>
<td class="ui-pg-button ui-corner-all" title="" id="del_grid-table" action="<?php echo url('Area/deletelists');?>" data-original-title="Delete selected row">
<div class="ui-pg-div">
<span class="ui-icon ace-icon fa fa-trash-o red"></span></div>
</td>
</tr>
</tbody>
</table>
{$page}
</div>

	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){

});
</script>
{/block}
