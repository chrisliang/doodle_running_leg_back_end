{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">

			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					添加配送区域
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" method="post" action="<?php echo url('Area/add_section_handler');?>">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 选择地区： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<select name="province" id="province_id" required="required">
							<option value="0">请选择省份</option>
							{foreach name="province" item="v"}
								<option value="{$v.id}">{$v.name}</option>
							{/foreach}
							</select>
							<select name="city" id="city_id" required="required">
							<option value="0">请选择城市</option>
							</select>
							<select name="area" id="area_id" style="display: none;">
							<option value="0">请选择区域</option>
							</select>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 配送区域名称：  </label>
						<div class="col-sm-10">
							<input type="text" name="section" id="section" placeholder="输入配送区域名称" class="col-xs-10 col-sm-4" required="required"/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 是否启用： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="status" id="status" value="1" checked="checked" class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div>
					<div class="space-4"></div>
										
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('#province_id').change(function(){
		var province_id  = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo url('Common/get_child_address');?>',
			dataType:'json',
			data : {pid:province_id},
			success:function(data){
				var data = '<option value="0">请选择城市</option>'+data;
				$('#city_id').html(data);
				var data = '<option value="0">请选择区域</option>';
				$('#area_id').html(data);
			},
		});
	});
	$('#city_id').change(function(){
		var city_id = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo url('Common/get_child_address');?>',
			dataType:'json',
			data : {pid:city_id},
			success:function(data){
				var data = '<option value="0">请选择区域</option>'+data;
				$('#area_id').html(data);
			},
		});
	});
});
</script>
{/block}