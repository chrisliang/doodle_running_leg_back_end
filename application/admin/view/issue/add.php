{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					添加原因
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" name="add" method="post" action="<?php echo url('run_add');?>">

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 标题：  </label>
						<div class="col-sm-10">
							<input type="text" name="title" id="title" placeholder="输入标题名" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 原因类型： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<label for="type">
							<input name="type" id="type" value="1" checked="checked" class="ace ace-switch ace-switch-4 btn-flat" type="radio" />
							<span class="lbl">&nbsp;订单不结算配送原因</span></label>
							&nbsp; &nbsp; &nbsp;
							<label for="type2">
							<input name="type" id="type2" value="2" class="ace ace-switch ace-switch-4 btn-flat" type="radio" />
							<span class="lbl">&nbsp;更换配送员原因</span>
							</label>
						</div>
						
					</div>
					<div class="space-4"></div>

					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
