<div class="hidden">
<button data-target="#sidebar2" data-toggle="collapse" type="button" class="pull-left navbar-toggle collapsed">
	<span class="sr-only">Toggle sidebar</span>

	<i class="ace-icon fa fa-dashboard white bigger-125"></i>
</button>

<div id="sidebar2" class="sidebar h-sidebar navbar-collapse collapse sidebar-fixed">
	<ul class="nav nav-list">
		<?php if (isset($top_menu)){ foreach ($top_menu as $key => $value) {?>
		<li class="hover">
			<a <?php if (strtolower(CONTROLLER_NAME.'/'.ACTION_NAME) == strtolower($value['node'])) {echo 'style="color:#f60;font-size:14px;font-weight:bold;"';} ?> href="<?php echo url($value['node']);?>">
				<span class="menu-text"><?php echo $value['title'];?></span>
			</a>
			<b class="arrow"></b>
		</li>
		<?php } }?>
	</ul><!-- /.nav-list -->
</div><!-- .sidebar -->
</div>