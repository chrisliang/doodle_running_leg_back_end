{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					新增广告
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" name="add" method="post" action="<?php echo url('ads/add');?>">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 广告城市： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<select name="areaId1" id="province_id">
							<option value="0">请选择省份</option>
							{foreach name="province" item="v"}
								<option value="{$v.areaId}">{$v.areaName}</option>
							{/foreach}
							</select>
							<select name="areaId2" id="city_id">
							<option value="0">请选择城市</option>
							</select>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 广告位置： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<select name="adPositionId" id="adPositionId" >
					            <option value='0'>微帮</option>
					            <!-- <option value='1' >云购</option> -->
					            <option value='2'>积分商城</option>
					            <option value='3'>新店来袭</option>
							</select>
						</div>
						(不选则默认整个商城)
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 广告标题：  </label>
						<div class="col-sm-10">
							<input type="text" name="adName" id="adName" placeholder="广告标题" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 广告图片： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="file" name="adFile" id="file0" />
								浏览
							</a>
							<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
								撤销
							</a>&nbsp;&nbsp;
							<span class="lbl">&nbsp;&nbsp;上传前先用PS处理成等比例图片后上传，最后都统一比例 ：1400px*300px<br /></span>
							<span class="lbl">&nbsp;&nbsp;<img src="__IMG__/no_img.jpg" width="420" height="90" id="img0" ></span>

						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> PC跳转网址：  </label>
						<div class="col-sm-10">
							<input type="text" name="adURL" id="adURL" placeholder="跳转网址" class="col-xs-10 col-sm-4" />
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> APP跳转：  </label>
						<div class="col-sm-10">
							<input type="text" name="adURLApp" id="adURLApp" placeholder="APP跳转" class="col-xs-10 col-sm-4" />(S|1 或 G|1 S代表店铺G代表商品，|后面的1分别是店铺或商品的ID)
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 广告日期：  </label>
						<div class="col-sm-10">
							<div class="input-group date form_date col-sm-4" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
								<input size="16" name="adStartDate" type="text" class="form-control" placeholder="开始时间">
                      			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    		</div>
		                    <div class="input-group date form_date3 col-sm-4" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input3" data-link-format="yyyy-mm-dd">
		                        <input size="16" name="adEndDate" type="text" class="form-control" placeholder="结束时间">
		                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
		                    </div>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 广告排序号：  </label>
						<div class="col-sm-10">
							<input type="text" name="adSort" id="adSort" placeholder="排序号" class="col-xs-10 col-sm-4" />
						</div>
					</div>
					<div class="space-4"></div>
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<a class="btn" href="javascript:window.history.back();">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								返回
							</a>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<link href="__PUBLIC__/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="__PUBLIC__/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="__PUBLIC__/datetimepicker/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript">
$(function(){
	$('#province_id').change(function(){
		var province_id  = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo url('ads/getCity');?>',
			dataType:'json',
			data : {pid:province_id},
			success:function(data){
				if(data.status==1){
					console.log(data);
					var html = '<option value="0">请选择城市</option>'+data.html;
					console.log(html);
					$('#city_id').html(html);
				}
			},
		});
	});

	$('.form_date').datetimepicker({
            language:  'zh-CN',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn:  1,
            timepicker:false,    //关闭时间选项
            yearStart:2016,      //设置最小年份
            yearEnd:2050,        //设置最大年份
            minDate: 0,          //最小日期
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
      $('.form_date3').datetimepicker({
            language:  'zh-CN',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn:  1,
            timepicker:false,    //关闭时间选项
            yearStart:2016,      //设置最小年份
            yearEnd:2050,        //设置最大年份
            minDate: 0,          //最小日期
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });

});
</script>
{/block}