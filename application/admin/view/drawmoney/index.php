{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<!-- 
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="post" action="">
		<label class="inline">
			<span class="lbl">店铺编号</span>
		</label>
		<input class="input" name="shopSn" value="" placeholder="请输入店铺编号" type="text">
		<label class="inline">
			<span class="lbl">店铺名称</span>
		</label>
		<input class="input" name="shopName" value="" placeholder="请输入店铺名称" type="text">
		<label class="inline">
			<span class="lbl">店铺电话</span>	
		</label>
		<input class="input" name="shopTel" value="" placeholder="请输入店铺电话" type="text">
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
-->
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>UID</th>
			<th>手机号码</th>
			<th>真实姓名</th>
			<th>银行名称</th>
			<th>开户地址</th>
			<th>银行卡号</th>
			<th>提现金额</th>
			<th>提现时间</th>
			<th>状态</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($lists))foreach ($lists as $key => $value) { ?>
		<tr>
			<td>{$value.userId}</td>
			<td>{$value.mobile}</td>
			<td>{$value.bank_user_name}</td>
			<td>{$value.bank_name}</td>
			<td>{$value.bank_access}</td>
			<td>{$value.bank_num}</td>
			<td>{$value.apply_price}</td>
			<td>{$value.time|date='Y-m-d H:i:s',###}</td>
			<td>
				<?php if ($value['status'] == 1){
					echo '处理中';
				}elseif ($value['status'] == 2){
					echo '已完成';
				}elseif ($value['status'] == 3){
					echo '已拒绝';
				}else{
					echo '待处理';
				}?>
			</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
				<?php if ($value['status'] != 3){ ?>
				<button class="btn btn-danger btn-xs ajaxStatus" action="{:url('closes')}" data-id="{$value.id}"><i class="ace-icon bigger-110"></i>拒绝</button>
				<?php } if ($value['status'] != 3){ ?>
				<button class="btn btn-danger btn-xs ajaxStatus" action="{:url('action_work')}" data-id="{$value.id}"><i class="ace-icon bigger-110"></i>处理中</button>
				<?php }?>
				<?php if ($value['status'] == 1){ ?>
				<button class="btn btn-danger btn-xs ajaxStatus" action="{:url('finish')}" data-id="{$value.id}"><i class="ace-icon bigger-110"></i>完成</button>
           		<?php }?>
            </div>
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
<?php if (isset($page)):?>

{if condition="$page"}
<div class="pager">
{$page}
</div>
{/if}

<?php endif;?>
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('.closes').click(function(){
		var shopid = $(this).attr('data-id');
		if(shopid){
		    var index = layer.open({
		        type: 2,
		        title: '设置配送区域',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['800px', '500px'],
		        content: '<?php echo url('setting');?>?shopid='+shopid
		    });
		}
	});
});
</script>
{/block}
