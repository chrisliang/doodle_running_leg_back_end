{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="get" action="">
		<label class="inline">
			<span class="lbl">会员账号</span>
		</label>
		<input class="input" name="loginName" value="{$loginName}" placeholder="请输入账号" type="text">
		<label class="inline">
			<span class="lbl">会员类型</span>
		</label>
		<select name="userType" class="form-control" id="userType">
		<option value="-1">全部</option>
		<option value="1" <?php if ($userType == 1) echo 'selected="selected"';?>>商家会员</option>
		<option value="0" <?php if ($userType == 0) echo 'selected="selected"';?>>普通会员</option>
		</select>
		<label class="inline">
			<span class="lbl">账号状态</span>
		</label>
		<select name="userStatus" class="form-control" id="userStatus">
		<option value="-1">全部</option>
		<option value="1" <?php if ($userStatus == 1) echo 'selected="selected"';?>>启用</option>
		<option value="0" <?php if ($userStatus == 0) echo 'selected="selected"';?>>停用</option>
		</select>
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
           <th width='10'>&nbsp;</th>
           <th width='80'>账号</th>
           <th width='30'>安全码</th>
           <th width='80'>用户名</th>
           <th width='90'>手机号码</th>
           <th width='100'>电子邮箱</th> 
           <th width='100'>最后登录时间</th>
           <th width='40'>状态</th>
           <th width='40'>操作</th>
         </tr>
	</thead>

	<tbody>
	<?php if(isset($list['data']))foreach ($list['data'] as $key => $value) { ?>
		<tr>
           <td>{$key+1}</td>
           <td>{$value['loginName']}</td>
           <td>{$value['loginSecret']}&nbsp;</td>
           <td>{$value['userName']}&nbsp;</td>
           <td>{$value['userPhone']}&nbsp;</td>
           <td>{$value['userEmail']}&nbsp;</td>
           <td>{$value['lastTime']}&nbsp;</td>
           <td>
           	<div id="data_{$value['userId']}"  style="cursor:pointer;">
           	{if condition ="$value['userStatus']==0"}
				<span data-status="{$value['userStatus']}" onclick="toggleStatus({$value['userId']},this)">停用</span>
           	{else/}
				<span data-status="{$value['userStatus']}" onclick="toggleStatus({$value['userId']},this)">启用</span>
			{/if}
            </div>
           </td>
		   <td>
			<div class="hidden-sm hidden-xs action-buttons">
			<button class="btn btn-default glyphicon glyphicon-pencil" data-toggle="modal" data-target="#myModal">修改</button>
			</div>
		   </td>
		</tr>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">
                        编辑账户信息
                    </h4>
                </div>

                <div class="modal-body">
                    <form class="form-horizontal" role="form" method="post" onsubmit="return false;">
                        <div class="form-group">
                            <label for="loginName" class="col-sm-3 control-label">账号</label>
                            <div class="col-sm-9">
                            	<input type="hidden" name="id" id="id" value="{$value['userId']}">
                  				<input type="text" class="form-control" id="loginName" name="loginName" value="{$value['loginName']}" readonly="true">
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="loginPwd" class="col-sm-3 control-label">密码</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="loginPwd" value="" id="loginPwd" placeholder="为空不修改">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="payPwd" class="col-sm-3 control-label">支付密码</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="payPwd" value="" id="payPwd" placeholder="为空不修改">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="userStatus" class="col-sm-3 control-label">会员状态</label>
                            <div class="col-sm-9">
                            {if condition = "$value['userStatus']==1"}
                            	<input type="radio" name="userStatus" id="userStatus" checked="true" value="1">启用
								<input type="radio" name="userStatus" id="userStatus" value="0">停用
							{else /}
                            	<input type="radio" name="userStatus" id="userStatus" value="1">启用
								<input type="radio" name="userStatus" id="userStatus" checked="true" value="0">停用
							{/if}
                            </div>
                        </div>

		                <div class="modal-footer">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
		                    <button type="submit" class="btn btn-primary" onclick="check_form();">保存</button>
		                </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
	<?php }?>
	</tbody>
</table>
{if condition="$list['page']"}
<div class="pager">
{$list['page']}
</div>
{/if}

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('.orderDetail').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '订单详情',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('detail');?>?orderid='+orderId
		    });
		}
	});
	$('.sendOrder').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '派单',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('send_order');?>?orderid='+orderId
		    });
		}
	});
});
	
	//隐藏模态框
	$(function() {
	    $('#myModal').on('hide.bs.modal',function(){})
	});

	//提交模态框表单
	function check_form(obj){
		var id = $("#id").val();
		var loginName = $("#loginName").val();
		var loginPwd = $("#loginPwd").val();
		var payPwd = $("#payPwd").val();
		var userStatus = $("input[name='userStatus']:checked").val();
		console.log(userStatus);
		var url = "{:url('admin/user/editAccount')}";
		$.post(url,{loginName:loginName,loginPwd:loginPwd,payPwd:payPwd,id:id,userStatus:userStatus},function(e){
			if(e.status==1){
				location.reload()
			}else{
				return false
			}
		},'json');
	}

	//切换状态
	function toggleStatus(id,obj){
		var status = $(obj).attr('data-status');
		var id = id;
		var url = "{:url('admin/user/toggleStatus')}";
		$.post(url,{id:id,status:status},function(e){
			if(e.status==1){
				var message = e.state?'启用':'停用';
				var html = '<span data-status="'+e.state+'" onclick="toggleStatus('+e.id+',this)">'+message+'</span>';
				$("#data_"+e.id).html(html);
				layer.msg(e.msg);
			}else{
				layer.msg(e.msg);
				return false;
			}
		},'json')
	}

	//删除
   function del(id,type){
   		var msg = (type==1)?'该会员为商家会员，您确定要删除该商家信息吗？':'您确定要删除该会员信息吗?';
		layer.confirm(msg, {btn: ['取消', '确定'],btn2: function(index, layero){
		   //确定
			$.post("{:url('admin/User/del')}",{id:id},function(data){
				if(data.status=='1'){
					layer.msg(data.msg);
				}else{
					return false
				}
				},'json');
			}
		}, function(index){
		  //取消
		  parent.layer.close(index);
		});
   }
</script>
{/block}
