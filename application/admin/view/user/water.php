{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<form class="form-inline" method="post" action="">
		<div style="margin-bottom: 15px;">
			<label class="inline">
				<span class="lbl">会员账号</span>
			</label>
			<div class="input-group">
				<input type="text" name="userPhone" value="{$userPhone}" class="layui-input" id="userPhone">
			</div>
			<label class="inline">
				<span class="lbl">流水类型</span>
			</label>
			<div class="input-group">
				<select id="type" name="type" style="min-width: 158px;">
					<option value="-1">请选择</option>
					<option value="6" {if condition="$type == 6"}selected{/if}>发布需求</option>
					<option value="2" {if condition="$type == 2"}selected{/if}>取消需求</option>
					<option value="3" {if condition="$type == 3"}selected{/if}>余额充值</option>
					<option value="4" {if condition="$type == 4"}selected{/if}>余额提现</option>
				</select>
			</div>
			<label class="inline">
				<span class="lbl">支付方式</span>
			</label>
			<div class="input-group">
				<select id="payWay" name="payWay" style="min-width: 158px;">
					<option value="-1">请选择</option>
					<option value="1" {if condition="$payWay == 1"}selected{/if}>支付宝</option>
					<option value="2" {if condition="$payWay == 2"}selected{/if}>微信</option>
					<option value="3" {if condition="$payWay == 3"}selected{/if}>余额</option>
				</select>
			</div>
		</div>
		<div>
			<label class="inline">
				<span class="lbl">日期区间</span>
			</label>
			<div class="input-group">
				<input type="text" name="startDate" value="{$startDate}" class="layui-input" id="test1">
	<!-- 			<span class="input-group-addon">
					<i class="fa fa-calendar bigger-110"></i>
				</span> -->
			</div>
			<label class="inline">
				<span class="lbl">-</span>
			</label>
					<div class="input-group">
				<input type="text" name="endDate" value="{$endDate}" class="layui-input" id="test2">
				<!-- <span class="input-group-addon">
					<i class="fa fa-calendar bigger-110"></i>
				</span> -->
			</div>
			
			<button type="submit" name="query" class="btn btn-info btn-sm">
				<i class="ace-icon glyphicon glyphicon-search"></i>查询
			</button>
		</div>
</form>
<div style="margin-top: 10px;">
<p style="color: red;">历史充值总额：{$history_total_recharge|round=###,2}   当前时间区间充值总额：{$current_total_recharge|round=###,2}</p>
<p style="color: red;">历史消费总额：{$history_total_expend|round=###,2}   当前时间区间消费总额：{$current_total_expend|round=###,2}</p>
	
</div>

<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>序号</th>
			<th>会员账号</th>
			<th>流水类型</th>
			<th>金额</th>
			<th>平台订单ID</th>
			<th>第三方支付流水账单号</th>
			<th>支付方式</th>
			<th>创建时间</th>
		</tr>
	</thead>

	<tbody>
		<if condition="isset($lists)">
		{foreach name="lists" item="v"}
		<tr id="tr">
			<td>{$v.id}</td>
			<td>{$v.userPhone}</td>
			<td class="hidden-480">
			{if condition="$v['type'] == 6"}
				发布需求
			{elseif condition="$v['type'] == 2"}
				取消需求
			{elseif condition="$v['type'] == 3"}
				余额充值
			{elseif condition="$v['type'] == 4"}
				余额提现
			{/if}
			</td>
			<td>{$v.money|round=###,2}</td>
			<td>{$v.orderNo}</td>
			<td>{$v.third_trade_no}</td>
			<td>{if condition="$v['payWay'] == 1"}
				支付宝
			{elseif condition="$v['payWay'] == 2"}
				微信
			{elseif condition="$v['payWay'] == 3"}
				余额
			{/if}</td>
			<td>{$v.time|date="Y-m-d H:i:s",###}</td>
		</tr>
		{/foreach}
	</if>
	</tbody>
</table>
<?php if (isset($page)):?>
<div class="pager">
<?php echo $page;?>
</div>
<?php endif;?>
	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script src="__PUBLIC__/layui/layui.js"></script>
<script src="__PUBLIC__/laydate/laydate.js"></script>
<link rel="stylesheet" href="__PUBLIC__/laydate/theme/default/laydate.css" />
<script type="text/javascript">

$(function(){
	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#test1',
		    type:'date' //指定元素
		  });
		  laydate.render({
			    elem: '#test2',
			    type:'date' //指定元素
		   });
	});
});
</script>
{/block}
