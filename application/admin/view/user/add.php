{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					添加会员
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" name="add_user" method="post" action="{:url('admin/user/addUser')}">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 账号：  </label>
						<div class="col-sm-10">
							<input type="text" name="loginName" id="loginName" placeholder="输入账号" value="" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 密码：  </label>
						<div class="col-sm-10">
							<input type="password" name="loginPwd" id="loginName" placeholder="输入密码" value="" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 用户名：  </label>
						<div class="col-sm-10">
							<input type="text" name="userName" id="userName" placeholder="输入用户名" value="" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 性别：  </label>
						<div class="col-sm-10">
							<label>
							<input type='radio' id='userSex1' name='userSex' checked="true" value='1'/>男
							</label>
							<label>
								<input type='radio' id='userSex2' name='userSex' value='2'/>女
							</label>
							<label>
								<input type='radio' id='userSex0' name='userSex' value='0'/>保密
							</label>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 手机号码：  </label>
						<div class="col-sm-10">
							<input type="text" name="userPhone" id="userPhone" placeholder="请输入手机号码" value="" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 电子邮箱：  </label>
						<div class="col-sm-10">
							<input type="text" name="userEmail" id="userEmail" placeholder="请输入电子邮箱" value="" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 会员QQ:  </label>
						<div class="col-sm-10">
							<input type="text" name="userQQ" id="userQQ" placeholder="输入QQ账号" value="" class="col-xs-10 col-sm-4" />
						</div>
					</div>
					<div class="space-4"></div>
<!-- 					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 会员历史积分：  </label>
						<div class="col-sm-10">
							<input type="text" name="userTotalScore" id="userName" placeholder="输入历史积分" value="" class="col-xs-10 col-sm-4" />
						</div>
					</div>
					<div class="space-4"></div> -->
<!-- 					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 会员类型： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="userType" id="userType" value="1" checked="checked" class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div> -->
<!-- 					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 会员类型：  </label>
						<div class="col-sm-10">
							<label>
							<input type='radio' id='userSex1' name='userType' checked="true" value='1'/>
							</label>
							<label>
								<input type='radio' id='userSex2' name='userType' value='2'/>
							</label>
							<label>
						</div>
					</div> -->
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 是否启用： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="userStatus" id="userStatus" value="1" checked="checked" class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 头像上传： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_face" value=""/>
								<input type="file" name="user_face" id="file0" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="__IMG__/no_img.jpg" width="100" height="70" id="img0" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;上传前先用PS处理成等比例图片后上传，最后都统一比例 ：100px*100px<br />
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 营业执照： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_businessLicenseUrl" value=""/>
								<input type="file" name="user_businessLicenseUrl" id="file1" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="__IMG__/no_img.jpg" width="300" height="280" id="img1" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;请注意营业执照清晰度<br />
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 身份证正面照： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_idCardFrontUrl" value=""/>
								<input type="file" name="user_idCardFrontUrl" id="file2" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="__IMG__/no_img.jpg" width="300" height="280" id="img2" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;请注意身份证正面照清晰度<br />
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 身份证反面照： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_idCardVersoUrl" value=""/>
								<input type="file" name="user_idCardVersoUrl" id="file3" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="__IMG__/no_img.jpg" width="300" height="280" id="img3" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;请注意身份证反面照清晰度<br />
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 手持身份证照： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_handIdCardUrl" value=""/>
								<input type="file" name="user_handIdCardUrl" id="file4" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="__IMG__/no_img.jpg" width="300" height="280" id="img4" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;请注意手持身份证照清晰度<br />
						</span>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">

</script>
{/block}