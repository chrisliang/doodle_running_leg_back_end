{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					编辑会员
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" name="add_user" method="post" action="{:url('admin/user/edit')}">
					<input type="hidden" name="id" value="{$data.userId}"/>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 账号：  </label>
						<div class="col-sm-10">
							<input type="text" name="loginName" id="loginName" placeholder="输入手机号码" value="<?php echo $data['loginName'];?>" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 用户名：  </label>
						<div class="col-sm-10">
							<input type="text" name="userName" id="userName" placeholder="输入用户名" value="<?php echo $data['userName'];?>" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 性别：  </label>
						<div class="col-sm-10">
							<label>
							<input type='radio' id='userSex1' name='userSex' {if condition="$data['userSex'] eq 1 "}checked{/if} value='1'/>男
							</label>
							<label>
								<input type='radio' id='userSex2' name='userSex' {if condition="$data['userSex'] eq 2 "}checked{/if} value='2'/>女
							</label>
							<label>
								<input type='radio' id='userSex0' name='userSex' {if condition="$data['userSex'] eq 0 "}checked{/if} value='0'/>保密
							</label>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 手机号码：  </label>
						<div class="col-sm-10">
							<input type="text" name="userPhone" id="userPhone" placeholder="请输入手机号码" value="<?php echo $data['userPhone'];?>" class="col-xs-10 col-sm-4"/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 电子邮箱：  </label>
						<div class="col-sm-10">
							<input type="text" name="userEmail" id="userEmail" placeholder="请输入电子邮箱" value="<?php echo $data['userEmail'];?>" class="col-xs-10 col-sm-4"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 会员QQ：  </label>
						<div class="col-sm-10">
							<input type="text" name="userQQ" id="userQQ" placeholder="请输入QQ" value="<?php echo $data['userQQ'];?>" class="col-xs-10 col-sm-4"/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 头像上传： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_face" value="<?php echo $data['userPhoto'];?>"/>
								<input type="file" name="user_face" id="file0" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="{$data.userPhoto}" width="100" height="70" id="img0" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;上传前先用PS处理成等比例图片后上传，最后都统一比例 ：100px*100px<br />
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 营业执照： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_businessLicenseUrl" value="<?php echo $data['businessLicenseUrl'];?>"/>
								<input type="file" name="user_businessLicenseUrl" id="file1" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="{$data.businessLicenseUrl}" width="300" height="280" id="img1" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;请注意营业执照清晰度<br />
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 身份证正面照： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_idCardFrontUrl" value="<?php echo $data['idCardFrontUrl'];?>"/>
								<input type="file" name="user_idCardFrontUrl" id="file2" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="{$data.idCardFrontUrl}" width="300" height="280" id="img2" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;请注意身份证正面照清晰度<br />
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 身份证反面照： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_idCardVersoUrl" value="<?php echo $data['idCardVersoUrl'];?>"/>
								<input type="file" name="user_idCardVersoUrl" id="file3" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="{$data.idCardVersoUrl}" width="300" height="280" id="img3" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;请注意身份证反面照清晰度<br />
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 手持身份证照： </label>
						<div class="col-sm-10">
							<a href="javascript:;" class="file">
								<input type="hidden" name="org_user_handIdCardUrl" value="<?php echo $data['handIdCardUrl'];?>"/>
								<input type="file" name="user_handIdCardUrl" id="file4" />
								选择上传文件
							</a>
							<span class="lbl">&nbsp;&nbsp;<img src="{$data.handIdCardUrl}" width="300" height="280" id="img4" ></span>&nbsp;&nbsp;<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
							撤销上传
						</a>
						<span class="lbl">&nbsp;&nbsp;请注意手持身份证照清晰度<br />
						</span>
						</div>
					</div>

					<div class="space-4"></div>
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
							{if condition="$data['isAudit'] == 2"}
								<button class="btn btn-primary" type="button" onclick="pass({$data['userId']})">
								<i class="ace-icon fa fa-check bigger-110"></i>
								通过审核
								</button>
								<button class="btn btn-danger" type="button" onclick="refund({$data['userId']})">
								<i class="ace-icon fa fa-times bigger-110"></i>
								拒绝通过
								</button>
							{/if}
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('#province_id').change(function(){
		$('#checkbox_address').html('');
		$('#checkbox_address_group').hide();
		var province_id  = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo url('Common/get_child_address');?>',
			dataType:'json',
			data : {pid:province_id},
			success:function(data){
				var data = '<option value="0">请选择城市</option>'+data;
				$('#city_id').html(data);
				var data = '<option value="0">请选择区域</option>';
				$('#area_id').html(data);
			},
		});
	});
// 	$('#city_id').change(function(){
//		$('#checkbox_address').html('');
//		$('#checkbox_address_group').hide();
//		var city_id = $(this).val();
//		$.ajax({
//			type:'get',
//			url:'<?php echo url('Common/get_child_address');?>',
//			dataType:'json',
//			data : {pid:city_id},
//			success:function(data){
//				var data = '<option value="0">请选择区域</option>'+data;
//				$('#area_id').html(data);
//			},
//		});
//	});
//	$('#area_id').change(function(){
//		var area_id = $(this).val();
//		$.ajax({
//			type:'get',
//		url:'<?php echo url('Delivery/get_area');?>',
//			dataType:'json',
//			data : {area_id:area_id},
//			success:function(html){
//				$('#checkbox_address').html(html);
//				$('#checkbox_address_group').show();
//			},
//		});
//	});

$('#city_id').change(function(){
	var city_id = $(this).val();
	$.ajax({
		type:'get',
		url:'<?php echo url('Delivery/get_area');?>',
		dataType:'json',
		data : {city_id:city_id},
		success:function(html){
			$('#checkbox_address').html(html);
			$('#checkbox_address_group').show();
		},
	});
});
});
function pass($id){
	$.ajax({
		type:'get',
		url:'<?php echo url('User/audit');?>',
		dataType:'json',
		data : {id:$id},
		success:function(ret){
			console.log(ret);
			if (ret['status'] == 1) {
				alert(ret['msg']);
				location.reload();
			}else{
				alert(ret['msg']);
			}
		},
	});
}
function refund($id){
	$.ajax({
		type:'get',
		url:'<?php echo url('User/refund');?>',
		dataType:'json',
		data : {id:$id},
		success:function(ret){
			console.log(ret);
			if (ret['status'] == 1) {
				alert(ret['msg']);
				location.reload();
			}else{
				alert(ret['msg']);
			}
		},
	});
}
</script>
{/block}