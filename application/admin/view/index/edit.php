{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<ul id="myTab" class="nav nav-tabs">
			    <li class="active">
			        <a href="#home" data-toggle="tab">平台设置</a>
			    </li>
			    <li style="display: none;">
			    	<a href="#email" data-toggle="tab">邮件设置</a>
			    </li>
			    <li style="display: none;">
			    	<a href="#message" data-toggle="tab">短信设置</a>
			    </li>
			    <li style="display: none;">
			    	<a href="#qrcode" data-toggle="tab">APP二维码</a>
			    </li>
			</ul>
			<div id="myTabContent" class="tab-content">
			    <div class="tab-pane fade in active" id="home">
			    	<form class="form-horizontal ajaxForm" name="edit" method="post" action="{:url('admin/index/edit',array('option'=>'mallconfig'))}">
			    	{foreach name="list" item="vo"}
			    		{switch name="$vo.fieldType" }
							{case value="text"}
							<div class="form-group" style="{if condition="$vo.configId ==1 or $vo.configId ==2 or $vo.configId ==3"}display: none;{/if}" >
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									{if condition="$vo.configId ==34 or $vo.configId ==92 or $vo.configId ==93"}
										<input type="number" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4" />
									{/if}
									{if condition="$vo.configId !=34 and $vo.configId !=92 and $vo.configId !=93"}
										<input type="text" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4" />
									{/if}
									
									<span class="lbl" style="line-height: 34px;">{$vo.fieldTips}</span>
								</div>
							</div>
							<div class="space-4" style="display: none;"></div>
							{/case}
							{case value="radio"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10" style="margin-top: 6px;">
									<label>
									<input type='radio' id='' name='{$vo.fieldCode}' {if condition="$vo.fieldValue==1"}checked{/if} value='1'/>是
									</label>
									<label>
										<input type='radio' id='' name='{$vo.fieldCode}' {if condition="$vo.fieldValue==0"}checked{/if} value='0'/>否
									</label>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="textarea"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<textarea name='{$vo.fieldCode}' style='width:40%;height:200px;'>{$vo.fieldValue}</textarea>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="other"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}： </label>
								<div class="col-sm-10" style="padding-top:5px;">
									<select name="province" id="province{$vo.fieldCode}">
										<option value="0"{if condition="$vo.provinceId==0"}selected {/if}>请选择省份</option>
										{foreach name="province" item="pro"}
										<option value="{$pro.areaId}" {if condition="$vo.provinceId==$pro.areaId"}selected {/if}>{$pro.areaName}</option>
										{/foreach}
									</select>
									<select name="{$vo.fieldCode}" id="{$vo.fieldCode}">
										<option value="0" {if condition="$vo.fieldValue==0"}selected {/if}>请选择城市</option>
										{foreach name="city" item="pro"}
										<option value="{$pro.areaId}" {if condition="$vo.fieldValue==$pro.areaId"}selected {/if}>{$pro.areaName}</option>
										{/foreach}									
									</select>
								</div>
							</div>
							{/case}
							{case value="upload"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}： </label>
								<div class="col-sm-10">
									<a href="javascript:;" class="file">
										<input type="file" name="{$vo.fieldCode}" id="{$vo.fieldCode}" value="{$vo.fieldValue}" />
										浏览
									</a>
									<span class="lbl">&nbsp;&nbsp;<img src="/{$vo.fieldValue}" width="240" height="132" id="img{$vo.fieldCode}" ></span>
									<a href="javascript:;" onClick="return backpic{$vo.fieldCode}('__IMG__/no_img.jpg');" title="还原图片" class="file">
										撤销
									</a>&nbsp;&nbsp;
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{default /}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<input type="text" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4"/>
									<span class="lbl">{$vo.fieldTips}</span>
								</div>
							</div>
							<div class="space-4"></div>	
						{/switch}					
					{/foreach}
						<div class="clearfix">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									保存
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									重置
								</button>
							</div>
						</div>
					</form>
			    </div>
			    <div class="tab-pane fade" id="email">
			        <form class="form-horizontal ajaxForm" name="edit" method="post" action="{:url('admin/index/edit',array('option'=>'mallconfig'))}">
			    	{foreach name="child[0]" item="vo"}
			    		{switch name="$vo.fieldType" }
							{case value="text"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<input type="text" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4" />
									<span class="lbl">{$vo.fieldTips}</span>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="radio"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<label>
									<input type='radio' id='' name='{$vo.fieldCode}' {if condition="$vo.fieldValue==1"}checked{/if} value='1'/>是
									</label>
									<label>
										<input type='radio' id='' name='{$vo.fieldCode}' {if condition="$vo.fieldValue==0"}checked{/if} value='0'/>否
									</label>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="textarea"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<textarea name='{$vo.fieldCode}' style='width:40%;height:200px;'>{$vo.fieldValue}</textarea>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="other"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}： </label>
								<div class="col-sm-10" style="padding-top:5px;">
									<select name="province" id="province_id">
										<option value="0">请选择省份</option>
										{foreach name="province" item="pro"}
										<option value="{$pro.areaId}">{$pro.areaName}</option>
										{/foreach}
									</select>
									<select name="{$vo.fieldCode}">
									<option value="0">请选择省份</option>
									<option value="0">请选择省份</option>
									</select>
								</div>
							</div>
							{/case}
							{case value="upload"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}： </label>
								<div class="col-sm-10">
									<a href="javascript:;" class="file">
										<input type="file" name="{$vo.fieldCode}" id="{$vo.fieldCode}" value="{$vo.fieldValue}" />
										浏览
									</a>
									<span class="lbl">&nbsp;&nbsp;<img src="/{$vo.fieldValue}" width="240" height="132" id="img{$vo.fieldCode}" ></span>
									<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
										撤销
									</a>&nbsp;&nbsp;
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{default /}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<input type="text" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4"/>
									<span class="lbl">{$vo.fieldTips}</span>
								</div>
							</div>
							<div class="space-4"></div>	
						{/switch}					
					{/foreach}
						<div class="clearfix">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									保存
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									重置
								</button>
							</div>
						</div>
					</form>
			    </div>
			    <div class="tab-pane fade" id="message">
			        <form class="form-horizontal ajaxForm" name="edit" method="post" action="{:url('admin/index/edit',array('option'=>'mallconfig'))}">
			    	{foreach name="child[1]" item="vo"}
			    		{switch name="$vo.fieldType" }
							{case value="text"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<input type="text" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4" />
									<span class="lbl">{$vo.fieldTips}</span>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="radio"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<label>
									<input type='radio' id='' name='{$vo.fieldCode}' {if condition="$vo.fieldValue==1"}checked{/if} value='1'/>是
									</label>
									<label>
										<input type='radio' id='' name='{$vo.fieldCode}' {if condition="$vo.fieldValue==0"}checked{/if} value='0'/>否
									</label>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="textarea"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<textarea name='{$vo.fieldCode}' style='width:40%;height:200px;'>{$vo.fieldValue}</textarea>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="other"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}： </label>
								<div class="col-sm-10" style="padding-top:5px;">
									<select name="province" id="province_id">
										<option value="0">请选择省份</option>
										{foreach name="province" item="pro"}
										<option value="{$pro.areaId}">{$pro.areaName}</option>
										{/foreach}
									</select>
									<select name="{$vo.fieldCode}">
									<option value="0">请选择省份</option>
									<option value="0">请选择省份</option>
									</select>
								</div>
							</div>
							{/case}
							{case value="upload"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}： </label>
								<div class="col-sm-10">
									<a href="javascript:;" class="file">
										<input type="file" name="{$vo.fieldCode}" id="{$vo.fieldCode}" value="{$vo.fieldValue}" />
										浏览
									</a>
									<span class="lbl">&nbsp;&nbsp;<img src="/{$vo.fieldValue}" width="240" height="132" id="img{$vo.fieldCode}" ></span>
									<a href="javascript:;" onClick="return backpic('__IMG__/no_img.jpg');" title="还原图片" class="file">
										撤销
									</a>&nbsp;&nbsp;
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{default /}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<input type="text" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4"/>
									<span class="lbl">{$vo.fieldTips}</span>
								</div>
							</div>
							<div class="space-4"></div>	
						{/switch}					
					{/foreach}
						<div class="clearfix">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									保存
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									重置
								</button>
							</div>
						</div>
					</form>
			    </div>
			    <div class="tab-pane fade" id="qrcode">
			        <form class="form-horizontal ajaxForm" name="edit" method="post" action="{:url('admin/index/edit',array('option'=>'mallconfig'))}">
			    	{foreach name="child[2]" item="vo"}
			    		{switch name="$vo.fieldType" }
							{case value="text"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<input type="text" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4" />
									<span class="lbl">{$vo.fieldTips}</span>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="radio"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<label>
									<input type='radio' id='' name='{$vo.fieldCode}' {if condition="$vo.fieldValue==1"}checked{/if} value='1'/>是
									</label>
									<label>
										<input type='radio' id='' name='{$vo.fieldCode}' {if condition="$vo.fieldValue==0"}checked{/if} value='0'/>否
									</label>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="textarea"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<textarea name='{$vo.fieldCode}' style='width:40%;height:200px;'>{$vo.fieldValue}</textarea>
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{case value="other"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}： </label>
								<div class="col-sm-10" style="padding-top:5px;">
									<select name="province" id="province_id">
										<option value="0">请选择省份</option>
										{foreach name="province" item="pro"}
										<option value="{$pro.areaId}">{$pro.areaName}</option>
										{/foreach}
									</select>
									<select name="{$vo.fieldCode}">
									<option value="0">请选择省份</option>
									<option value="0">请选择省份</option>
									</select>
								</div>
							</div>
							{/case}
							{case value="upload"}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}： </label>
								<div class="col-sm-10">
									<a href="javascript:;" class="file">
										<input type="file" name="{$vo.fieldCode}" id="{$vo.fieldCode}" value="{$vo.fieldValue}" />
										浏览
									</a>
									<span class="lbl">&nbsp;&nbsp;<img src="/{$vo.fieldValue}" width="240" height="132" id="img{$vo.fieldCode}" ></span>
									<a href="javascript:;" onClick="return backpic{$vo.fieldCode}('__IMG__/no_img.jpg');" title="还原图片" class="file">
										撤销
									</a>&nbsp;&nbsp;
								</div>
							</div>
							<div class="space-4"></div>
							{/case}
							{default /}
							<div class="form-group">
								<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> {$vo.fieldName}：  </label>
								<div class="col-sm-10">
									<input type="text" name="{$vo.fieldCode}" id="" placeholder="" value="{$vo.fieldValue}" class="col-xs-10 col-sm-4"/>
									<span class="lbl">{$vo.fieldTips}</span>
								</div>
							</div>
							<div class="space-4"></div>	
						{/switch}					
					{/foreach}
						<div class="clearfix">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit">
									<i class="ace-icon fa fa-check bigger-110"></i>
									保存
								</button>

								&nbsp; &nbsp; &nbsp;
								<button class="btn" type="reset">
									<i class="ace-icon fa fa-undo bigger-110"></i>
									重置
								</button>
							</div>
						</div>
					</form>
			    </div>

			</div><!--.tab-content-->

		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<link href="__PUBLIC__/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="__PUBLIC__/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="__PUBLIC__/datetimepicker/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script src="__PUBLIC__/kindeditor/kindeditor.js"></script>
<script src="__PUBLIC__/kindeditor/lang/zh_CN.js"></script>
<script type="text/javascript">
$(function(){
	 //   KindEditor.ready(function(K) {
		// 	editor1 = K.create('textarea[name="articleContent"]', {
		// 		height:'350px',
		// 		allowFileManager : false,
		// 		allowImageUpload : true,
		// 		items:[
		// 		        'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
		// 		        'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
		// 		        'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
		// 		        'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
		// 		        'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
		// 		        'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|','image','table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
		// 		        'anchor', 'link', 'unlink', '|', 'about'
		// 		],
		// 		afterBlur: function(){ this.sync(); }
		// 	});
		// });
	$('#provincedefaultCity').change(function(){
		var province_id  = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo url('index/getCity');?>',
			dataType:'json',
			data : {pid:province_id},
			success:function(data){
				if(data.status==1){
					var html = '<option value="0">请选择城市</option>'+data.html;
					$('#defaultCity').html(html);
				}
			},
		});
	});
	// console.log($("input[type='file']").length)
	
	$('.form_date').datetimepicker({
            language:  'zh-CN',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn:  1,
            timepicker:false,    //关闭时间选项
            yearStart:2016,      //设置最小年份
            yearEnd:2050,        //设置最大年份
            minDate: 0,          //最小日期
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
      $('.form_date3').datetimepicker({
            language:  'zh-CN',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn:  1,
            timepicker:false,    //关闭时间选项
            yearStart:2016,      //设置最小年份
            yearEnd:2050,        //设置最大年份
            minDate: 0,          //最小日期
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });

});

{foreach name="upload" item="v"}
	//上传图片4
	$("#{$v.fieldCode}").change(function () {
	    var objUrl = getObjectURL(this.files[0]);
	    console.log(this.files);
	    console.log("objUrl = " + objUrl);
	    if (objUrl) {
	        $("#img{$v.fieldCode}").attr("src", objUrl);
	    }
	});
	//清空图片
	function backpic{$v.fieldCode}(picurl) {
	    $("#img{$v.fieldCode}").attr("src", picurl);//还原修改前的图片
	    $("input[name='{$v.fieldCode}']").val("");//清空文本框的值
	    $("input[name='oldcheckpic{$v.fieldCode}']").val(picurl);//清空文本框的值
	}

	//获取文件对象url
	function getObjectURL(file) {
	    var url = null;
	    if (window.createObjectURL != undefined) { // basic
	        $("#oldcheckpic{$v.fieldCode}").val("nopic");
	        url = window.createObjectURL(file);
	    } else if (window.URL != undefined) { // mozilla(firefox)
	        $("#oldcheckpic{$v.fieldCode}").val("nopic");
	        url = window.URL.createObjectURL(file);
	    } else if (window.webkitURL != undefined) { // webkit or chrome
	        $("#oldcheckpic{$v.fieldCode}").val("nopic");
	        url = window.webkitURL.createObjectURL(file);
	    }
	    return url;
	}
	{/foreach}
</script>
{/block}