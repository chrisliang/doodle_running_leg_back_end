{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
						<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					邮箱配置
				</small></h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
				
					{include file="public/top_menu"}


		<section class="content">

            <div class="tabbable">
				<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a data-toggle="tab" href="#home">邮箱配置</a>
				</li>
				<li>
					<a data-toggle="tab" href="#template">邮箱模板</a>
				</li>
				</ul>
				
				<div class="tab-content">
				
					<div id="home" class="tab-pane fade in active">
					<form class="form-horizontal ajaxForm" name="system_config" method="post" action="<?php echo url('edit_email_run');?>">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> SMTP服务器：  </label>
						<div class="col-sm-10">
							<input type="text" name="smtp_host" id="smtp_host" placeholder="输入SMTP服务器" value="{$smtp_host}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> SMTP服务器端口：  </label>
						<div class="col-sm-10">
							<input type="text" name="smtp_port" id="smtp_port" placeholder="输入SMTP服务器端口" value="{$smtp_port}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> SMTP服务器用户名：  </label>
						<div class="col-sm-10">
							<input type="text" name="smtp_user" id="smtp_user" placeholder="输入SMTP服务器用户名" value="{$smtp_user}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					
										<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> SMTP服务器密码：  </label>
						<div class="col-sm-10">
							<input type="text" name="smtp_pass" id="smtp_pass" placeholder="输入SMTP服务器密码" value="{$smtp_pass}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
									<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 发件人EMAIL：  </label>
						<div class="col-sm-10">
							<input type="text" name="from_email" id="from_email" placeholder="输入发件人EMAIL" value="{$from_email}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
						<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 发件人名称：  </label>
						<div class="col-sm-10">
							<input type="text" name="from_name" id="from_name" placeholder="输入发件人名称" value="{$from_name}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
						<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 回复EMAIL（留空则为发件人EMAIL）：  </label>
						<div class="col-sm-10">
							<input type="text" name="reply_email" id="reply_email" placeholder="输入回复EMAIL（留空则为发件人EMAIL）" value="{$reply_email}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
									<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 回复名称（留空则为发件人名称）：  </label>
						<div class="col-sm-10">
							<input type="text" name="reply_name" id="reply_name" placeholder="输入回复名称（留空则为发件人名称）" value="{$reply_name}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
									<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 测试邮箱：  </label>
						<div class="col-sm-10">
							<input type="text" name="test_email" id="test_email" placeholder="输入测试邮箱（可留空）" class="test_email col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 开启发送邮箱： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="open_email" id="open_email" value="1" {if condition="$open_email"}checked="checked"{/if} class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div>
					<div class="space-4"></div>
										<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>	&nbsp; &nbsp; &nbsp;						
							<a href="javascript:;" class="test_email_btn btn btn-info">测试邮箱</a>
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
					</form>
					</div>
					
					<div id="template" class="tab-pane fade in">
					<form class="form-horizontal ajaxForm" name="system_config" method="post" action="<?php echo url('edittpl_email_run');?>">
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 用户邮件验证模板：  </label>
						<div class="col-sm-10">
							<textarea name="email_tpl_reg" id="form-field-11" class="autosize-transition form-control" style="max-width:612px;height:200px;resize:none;">{$email_reg_tpl.value}</textarea>
						</div>
					</div>
					<div class="space-4"></div>
						<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 找回密码模板：  </label>
						<div class="col-sm-10">
							<textarea name="email_tpl_find" id="form-field-11" class="autosize-transition form-control" style="max-width:612px;height:200px;resize:none;">{$email_find_tpl.value}</textarea>
						</div>
					</div>
					<div class="space-4"></div>
						<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
					</form>
					</div>
					
					
				</div>
				
			</div>

        </section>
					
					
					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('.test_email_btn').click(function(){
		var test_email = $('.test_email').val();
		if(test_email == ''){
			layer.msg('请输入测试邮箱', function(){});
			return false;
		}
		$.ajax({
			type: 'POST',
			url:'<?php echo url('test_email');?>',
			data:{email:test_email},
			success:function(data){
				var data = $.parseJSON(data);
				layer.msg(data.msg, function(){});
			}
		});
	});
});
</script>
{/block}