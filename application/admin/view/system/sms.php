{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
						<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					短信接口配置
				</small></h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
				
					{include file="public/top_menu"}


		<section class="content">

            <div class="tabbable">
				<ul class="nav nav-tabs" id="myTab">
				<li class="active">
					<a data-toggle="tab" href="#home">短信接口配置</a>
				</li>
				<li>
					<a data-toggle="tab" href="#template">短信模板</a>
				</li>
				</ul>
				
				<div class="tab-content">
				
					<div id="home" class="tab-pane fade in active">
					<form class="form-horizontal ajaxForm" name="system_config" method="post" action="<?php echo url('edit_sms_run');?>">
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 短信账户：  </label>
						<div class="col-sm-10">
							<input type="text" name="accountsid" id="accountsid" placeholder="输入短信账户" value="{$accountsid}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 账户TOKEN：  </label>
						<div class="col-sm-10">
							<input type="text" name="authtoken" id="authtoken" placeholder="输入账户TOKEN" value="{$authtoken}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> APPID：  </label>
						<div class="col-sm-10">
							<input type="text" name="appid" id="appid" placeholder="输入APPID" value="{$appid}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
										<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> UID：  </label>
						<div class="col-sm-10">
							<input type="text" name="uid" id="uid" placeholder="输入UID" value="{$uid}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
										<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 用户名：  </label>
						<div class="col-sm-10">
							<input type="text" name="mid" id="mid" placeholder="输入APPID" value="{$mid}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
										<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 密码：  </label>
						<div class="col-sm-10">
							<input type="text" name="mpass" id="mpass" placeholder="输入APPID" value="{$mpass}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
						<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 短信签名：  </label>
						<div class="col-sm-10">
							<input type="text" name="mqianming" id="mqianming" placeholder="输入短信签名" value="{$mqianming}" class="col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					
									<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 测试手机号码：  </label>
						<div class="col-sm-10">
							<input type="text" name="mobile" id="test_mobile" placeholder="输入手机号码（可留空）" value="{$mobile}" class="test_mobile col-xs-10 col-sm-7"/>
						</div>
					</div>
					<div class="space-4"></div>
					
										
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 测试短信内容：  </label>
						<div class="col-sm-10">
							<textarea name="content" id="form-field-11" class="autosize-transition form-control" style="max-width:612px;resize:none;">你现在正在使用人汇信息技术的短信接口，请您注意查收！【{$mqianming}】</textarea>
							<span class="lbl">&nbsp;70字内</span>	
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 开启短信功能： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="open_sms" id="open_sms" value="1" {if condition="$open_sms"}checked="checked"{/if} class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<p style="padding-left:120px;font-size:14px;">状态：{$text}</p>
					</div>
					<div class="space-4"></div>
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>&nbsp; &nbsp; &nbsp;							
							<a href="javascript:;" class="test_email_btn btn btn-info">测试短信</a>
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
					</form>
					</div>

					<div id="template" class="tab-pane fade in">
					<form class="form-horizontal ajaxForm" name="system_config" method="post" action="<?php echo url('edittpl_sms_run');?>">
			
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 验证模板：  </label>
						<div class="col-sm-10">
							<textarea name="sms_check_tpl" id="form-field-11" class="autosize-transition form-control" style="max-width:612px;resize:none;">{$sms_check_tpl.value}</textarea>
							<span class="lbl">&nbsp;70字内</span>	
						</div>
					</div>
					<div class="space-4"></div>
					
										<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 找回密码模板：  </label>
						<div class="col-sm-10">
							<textarea name="sms_find_tpl" id="form-field-11" class="autosize-transition form-control" style="max-width:612px;resize:none;">{$sms_find_tpl.value}</textarea>
							<span class="lbl">&nbsp;70字内</span>	
						</div>
					</div>
					<div class="space-4"></div>
					
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
					</form>
					</div>

					
				</div>
				
			</div>

        </section>
					
					
					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('.test_email_btn').click(function(){
		var test_mobile = $('#test_mobile').val();
		var content = $('textarea[name=content]').val();
		if(test_mobile == ''){
			layer.msg('请输入手机号码', function(){});
			return false;
		}
		$.ajax({
			type: 'POST',
			url:'<?php echo url('test_mobile');?>',
			data:{mobile:test_mobile,content:content},
			success:function(data){
				var data = $.parseJSON(data);
				layer.msg(data.msg, function(){});
			}
		});
	});
});
</script>
{/block}