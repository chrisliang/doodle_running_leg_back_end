{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<!--
<div class="page-header">
	<h1>Two menu </h1>
</div> /.page-header -->

<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="position-relative">
					<input class="ace" type="checkbox">
					<span class="lbl"></span>
				</label>
			</th>
			<th>名称</th>
			<th class="hidden-480">目录路径</th>
			<th>占用大小</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($lists))foreach ($lists as $key => $value) { ?>
		<tr>
			<td class="center">
				<label class="position-relative">
					<input class="ace" value="{$key}" name="checkbox[]" type="checkbox">
					<span class="lbl"></span>
				</label>
			</td>
			<td>{$value.title}</td>
			<td class="hidden-480">{$value.dir}</td>
			<td class="hidden-480">{$value.filesize}</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
<button class="btn btn-danger btn-sm delete-cache">
<i class="ace-icon fa fa-trash-o bigger-110"></i>删除
</button>

	
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	var key = '';
	$('.delete-cache').click(function(){
		$('td input[type=checkbox]').each(function(){
			if($(this).is(':checked')){
				key += $(this).val()+'|'
			}
		});
		if(key != ''){
			$.ajax({type:'post',url:'<?php echo url('run_clear');?>',data:{k:key},success:function(data){
				var data = $.parseJSON(data);
				if(data.code) window.location.reload();
			}});
		}
	});
});
</script>
{/block}
