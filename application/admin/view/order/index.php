{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="get" action="">
		<label class="inline">
			<span class="lbl">下单账号</span>
		</label>
		<input class="input" name="userPhone" value="{$userPhone}" placeholder="请输入账号" type="text">
		<label class="inline">
			<span class="lbl">下单时长</span>
		</label>
		<select name="timestamp" class="form-control" id="form-field-select-1">
		<option value="0">全部订单</option>
		<option value="5" <?php if ($selected1) echo 'selected="selected"';?>>不足5分钟</option>
		<option value="10" <?php if ($selected2) echo 'selected="selected"';?>>5-10分钟</option>
		<option value="11" <?php if ($selected3) echo 'selected="selected"';?>>大于10分钟</option>
		</select>
		
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>UID</th>
			<th>商铺ID</th>
			<th>订单ID</th>
			<th>订单号</th>
			<th>配送费</th>
			<th>下单账号</th>
			<th>下单时间</th>
			<th>预计到达时间</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($lists))foreach ($lists as $key => $value) { ?>
		<tr>
			<td>{$value.userId}</td>
			<th>{$value.shopId}</th>
			<td>{$value.orderId}</td>
			<td>{$value.orderNo}</td>
			<td>{$value.deliverMoney}</td>
			<td>{$value.userPhone}</td>
			<td>{$value.createTime}</td>
			<td>{$value.requireTime}</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
					<button class="btn btn-danger btn-xs orderDetail" data-id="{$value.orderId}"><i class="ace-icon fa fa-eye bigger-110"></i>订单详情</button>
                  	<button class="btn btn-danger btn-xs sendOrder" data-id="{$value.orderId}"><i class="ace-icon fa fa-share bigger-110"></i>派单</button>
            </div>
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
{if condition="$page"}
<div class="pager">
{$page}
</div>
{/if}

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('.orderDetail').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '订单详情',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('detail');?>?orderid='+orderId
		    });
		}
	});
	$('.sendOrder').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '派单',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('send_order');?>?orderid='+orderId
		    });
		}
	});
});
</script>
{/block}
