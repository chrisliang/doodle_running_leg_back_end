{extend name="public/base" /}
{block name="main"}
<style>.list-unstyled li{padding:5px 0px;}#sidebar2{display:none !important;}.main-container,.page-content{margin-top:0px !important;padding-top:5px !important;}</style>
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<div id="user-profile-1" class="user-profile row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="space-6"></div>

<h5 style="padding:10px;">订单状态</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>   
		{$createTime} 用户已下单  等待接单
	</li>
	<?php if (!empty($isData)){ ?>
<li>
		<i class="ace-icon fa fa-caret-right blue"></i>   
		<?php echo date('Y-m-d H:i:s',$isData['time']);?>
	</li>
	<?php } ?>
</ul>
<div class="hr hr2"></div>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		下单时间：{$createTime}
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		已下单时长：<?php echo _put_time(strtotime($createTime));?>
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		配送费：{$deliverMoney}元
	</li>
</ul>
<div class="hr hr2"></div>

<h5 style="padding:10px;">配送信息</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		取单地址：{$shopAddress}
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		收货地址：{$userAddress}
	</li>
			<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		送达时间：{$requireTime}
	</li>
			<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		收货人：{$userName}
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		商家联系号码：{$shopTel}
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		客户联系号码：{$userPhone}
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		买家账号：{$userName}
	</li>
</ul>
<div class="hr hr2"></div>

<h5 style="padding:10px;">商品信息</h5>

<div class="profile-user-info profile-user-info-striped">
	<div class="profile-info-row">
		<div class="profile-info-value"><strong>商品名称</strong></div>
		<div class="profile-info-value"><strong>数量</strong></div>
		<div class="profile-info-value"><strong>价格</strong></div>
	</div>
	{foreach name="lists" item="v"}
	<div class="profile-info-row">
		<div class="profile-info-value">{$v.goodsName}</div>
		<div class="profile-info-value">{$v.goodsNums}</div>
		<div class="profile-info-value">{$v.goodsPrice}</div>
	</div>
	{/foreach}
	<?php if (!empty($coupon)){ ?>
	<div class="profile-info-row">
		<div class="profile-info-value"><strong><?php echo $coupon['name'];?></strong></div>
		<div class="profile-info-value"><strong></strong></div>
		<div class="profile-info-value"><strong>- ¥<?php echo $couponMoney;?></strong></div>
	</div>
	<?php }?>
</div>

<div class="hr hr2"></div>

<h5 style="padding:10px;">订单信息</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		订单总金额：<strong style="font-size: 18px;color:#f60;">{$needPay}</strong> 元
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		商品订单号：{$orderNo}
	</li>
</ul>
<div class="hr hr2"></div>




</div>
</div>

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('#orderDetail').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '订单详情',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('detail');?>?orderid='+orderId
		    });
		}
	});
});
</script>
{/block}
