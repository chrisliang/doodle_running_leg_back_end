{extend name="public/base" /}
{block name="main"}
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<div class="widget-body">
<div class="widget-main">
	<form class="form-inline" method="get" action="">
		<label class="inline">
			<span class="lbl">下单账号</span>
		</label>
		<input class="input" name="userPhone" value="{$userPhone}" placeholder="请输入账号" type="text">
		<label class="inline">
			<span class="lbl">配送员账号/姓名</span>
		</label>
		<input class="input" name="account" value="{$account}" placeholder="请输入账号/姓名" type="text">
		<label class="inline">
			<span class="lbl">配送状态</span>
		</label>
		<select name="timeout" class="form-control" id="form-field-select-1">
		<option value="1" <?php if ($selected == 1) echo 'selected="selected"';?>>正常</option>
		<option value="2" <?php if ($selected == 2) echo 'selected="selected"';?>>超时</option>
		</select>
		<label class="inline">
			<span class="lbl">结算状态</span>
		</label>
		<select name="countstatus" class="form-control" id="form-field-select-1">
		<option value="1" <?php if ($countstatus == 1) echo 'selected="selected"';?>>正常结算</option>
		<option value="2" <?php if ($countstatus == 2) echo 'selected="selected"';?>>不结算</option>
		</select>
		<button type="submit" class="btn btn-info btn-sm">
			<i class="ace-icon glyphicon glyphicon-search"></i>查询
		</button>
	</form>
	</div>
</div>
<table id="sample-table-1" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>UID</th>
			<th>商铺ID</th>
			<th>订单ID</th>
			<th>订单号</th>
			<th>配送费</th>
			<th>下单账号</th>
			<th>下单时间</th>
			<th>配送员</th>
			<th>接单时间</th>
			<th>配送状态</th>
			<th>操作</th>
		</tr>
	</thead>

	<tbody>
		<?php if(isset($lists))foreach ($lists as $key => $value) { ?>
		<tr>
			<td>{$value.userId}</td>
			<th>{$value.shopId}</th>
			<td>{$value.orderId}</td>
			<td>{$value.orderNo}</td>
			<td>{$value.deliverMoney}</td>
			<td>{$value.userPhone}</td>
			<td>{$value.createTime}</td>
			<td>{if condition="isset($user[$value['delivery_uid']])"}{$user[$value['delivery_uid']]['truename']}{/if}</td>
			<td>{$value.time|date='Y-m-d H:i:s',###}</td>
			<td>{if condition="$value['timeout'] eq 1"}正常{else/}超时{/if}</td>
			<td>
			<div class="hidden-sm hidden-xs action-buttons">
					<button class="btn btn-danger btn-xs orderDetail" data-id="{$value.orderId}"><i class="ace-icon fa fa-eye bigger-110"></i>订单详情</button>
            		{if condition="!isset($not_show)"}
            		<button class="btn btn-danger btn-xs notCount" data-id="{$value.id}">订单不结算配送费</button>
            		{/if}
            		{if condition="!isset($not_handler)"}
            		<button class="btn btn-danger btn-xs sendOrder" data-id="{$value.id}">更换配送员</button>
            		{/if}
            </div>
			</td>
		</tr>
<?php }?>
		
	</tbody>
</table>
{if condition="$page"}
<div class="pager">
{$page}
</div>
{/if}

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('.orderDetail').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '订单详情',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('detail');?>?orderid='+orderId
		    });
		}
	});
	{if condition="!isset($not_show)"}
	$('.notCount').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '修改订单状态',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['493px', '300px'],
		        content: '<?php echo url('notcount');?>?orderid='+orderId
		    });
		}
	});
	{/if}
		{if condition="!isset($not_handler)"}
	$('.sendOrder').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '改换配送员',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('updateuser');?>?orderid='+orderId
		    });
		}
	});
	{/if}
});
</script>
{/block}
