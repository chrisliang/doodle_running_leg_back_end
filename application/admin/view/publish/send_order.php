{extend name="public/base" /}
{block name="main"}
<style>.list-unstyled li{padding:5px 0px;}#sidebar2{display:none !important;}.main-container,.page-content{margin-top:0px !important;padding-top:5px !important;}</style>
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<form class="form-horizontal ajaxForm" method="post" action="<?php echo url('send_order_run');?>">
<div id="user-profile-1" class="user-profile row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="space-6"></div>
<input type="hidden" name="orderId" value="{$orderInfo.id}"/>
<h5 style="padding:10px;">选择一个配送员</h5>
<div class="hr hr2"></div>
<div class="radio">
	{foreach name="userlist" item="v"}
	<label>
		<input name="user_id" value="{$v.id}" class="ace" type="radio">
		<span class="lbl"> [{$v.truename}][{$v.mobile}]</span>
	</label>
	{/foreach}
</div>

<div class="hr hr2"></div>
<div class="space-10"></div>			
<div class="clearfix">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
			<i class="ace-icon fa fa-check bigger-110"></i>
			派发
		</button>
	</div>
</div>
</div>
</div>
</form>
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}

{/block}
