{extend name="public/base" /}
{block name="main"}
<style>.list-unstyled li{padding:5px 0px;}#sidebar2{display:none !important;}.main-container,.page-content{margin-top:0px !important;padding-top:5px !important;}</style>
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}
<form class="form-horizontal ajaxForm" method="post" action="<?php echo url('run_user');?>">
<div id="user-profile-1" class="user-profile row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="space-6"></div>
<input type="hidden" name="orderId" value="<?php echo $id;?>"/>
<input type="hidden" name="uid" value="<?php echo $userId;?>"/>
<div class="form-group">
<h5 style="padding:10px;">选择一个配送员</h5>
<div class="hr hr2"></div>
<div class="radio">
	{foreach name="userlist" item="v"}
	<label>
		<input name="user_id" value="{$v.id}" {if condition="$userId == $v['id']"}checked="checked"{/if} class="ace" type="radio">
		<span class="lbl"> [{$v.truename}][{$v.mobile}]   {if condition="$userId == $v['id']"}<strong>(当前配送员)</strong>{/if}</span>
	</label>
	{/foreach}
</div>
</div>
<div class="form-group">
<div class="col-sm-10" style="padding-top:5px;">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 选择原因： </label>
		<select name="type" id="type_id" required="required">
		<option value="">请选择原因</option>
		{foreach name="issue" item="v"}
			<option value="{$v.id}">{$v.title}</option>
		{/foreach}
		</select>
	</div>
</div>
<div class="space-4"></div>
<div class="form-group">
	<div class="col-sm-10" style="padding-top:5px;">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 消息内容： </label>
		<textarea name="content" id="form-field-11" class="autosize-transition form-control" style="max-width:612px;resize:none;"></textarea>
	</div>
</div>
<div class="space-4"></div>
<div class="hr hr2"></div>
<div class="space-10"></div>			
<div class="clearfix">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">确认</button>
	</div>
</div>
</div>
</div>
</form>
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}

{/block}
