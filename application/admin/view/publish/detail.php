{extend name="public/base" /}
{block name="main"}
<style>.list-unstyled li{padding:5px 0px;}#sidebar2{display:none !important;}.main-container,.page-content{margin-top:0px !important;padding-top:5px !important;}</style>
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
			
<!-- #section:settings.box -->
{include file="public/setting"}
<!-- /section:settings.box -->
<div class="row">
<div class="col-xs-12">
{include file="public/top_menu"}

<div id="user-profile-1" class="user-profile row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="space-6"></div>

<h5 style="padding:10px;">订单状态</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>   
		{$time|date='Y-m-d H:i:s',###} 用户已下单  等待接单
	</li>
</ul>
<div class="hr hr2"></div>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		下单时间：{$time|date='Y-m-d H:i:s',###}
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		已下单时长：<?php echo _put_time($time);?>
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		配送费：{$money}元
	</li>
</ul>
<div class="hr hr2"></div>

<h5 style="padding:10px;">配送信息</h5>
<?php if ($type == 3){?>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		<font style="color:#f60;">买</font>：{$content['buy_address']}{$content['buy_number']} &nbsp;&nbsp;(购买地址)
	</li>
		
</ul>
<?php }else{?>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		<font style="color:#f60;">取</font>：{$content['go_address']}{$content['go_number']} &nbsp;&nbsp;(取货地址)
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系人：{$content['go_username']}
	</li>		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系电话：{$content['go_mobile']}
	</li>
</ul>
<?php }?>
<ul class="list-unstyled">
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		<font style="color:#f60;">送</font>：{$content['get_address']}{$content['get_number']} &nbsp;&nbsp;(目的地址)
	</li>
		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系人：{$content['get_username']}
	</li>		<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		联系电话：{$content['get_mobile']}
	</li>
	

</ul>

<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		送达时间：{$send_time|date='Y-m-d H:i:s',###}
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		下单账号：{$username}
	</li>
	
	<li style="display: none;">
		<i class="ace-icon fa fa-caret-right blue"></i>
		加急情况：{$content['urgent_text']}
	</li>
	<li style="display: none">
		<i class="ace-icon fa fa-caret-right blue"></i>
		车型：{$content['car_text']}
	</li>
	
</ul>

<div class="hr hr2"></div>

<h5 style="padding:10px;">订单信息</h5>
<ul class="list-unstyled">
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		订单总金额：<strong style="font-size: 18px;color:#f60;">{$money}</strong> 元
	</li>
	<li>
		<i class="ace-icon fa fa-caret-right blue"></i>
		需求订单号：{$id}
	</li>
</ul>
<div class="hr hr2"></div>




</div>
</div>

</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
{/block}
{block name="footer_static"}
<script type="text/javascript">
$(function(){
	$('#orderDetail').click(function(){
		var orderId = $(this).attr('data-id');
		if(orderId){
		    var index = layer.open({
		        type: 2,
		        title: '订单详情',
		        shadeClose: false,
		        shade: 0.2,
		        maxmin: true, //开启最大化最小化按钮
		        area: ['893px', '600px'],
		        content: '<?php echo url('detail');?>?orderid='+orderId
		    });
		}
	});
});
</script>
{/block}
