{extend name="public/base" /}
{block name="main"}
<div class="main-content">
	<div class="main-content-inner">
		<div class="page-content">
			
			<!-- #section:settings.box -->
			{include file="public/setting"}
			<!-- /section:settings.box -->
			
			<div class="page-header">
				<h1>您当前操作<small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					添加管理员
				</small></h1>
			</div>

			<div class="row">
				<div class="col-xs-12">
				{include file="public/top_menu"}

				<form class="form-horizontal ajaxForm" name="admin_group_add" method="post" action="<?php echo url('Manager/add_admin_handler');?>">

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 名称：  </label>
						<div class="col-sm-10">
							<input type="text" name="username" id="username" placeholder="输入管理员名称" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 密码：  </label>
						<div class="col-sm-10">
							<input type="password" name="password" id="password" placeholder="输入密码" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
															<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 真实姓名：  </label>
						<div class="col-sm-10">
							<input type="text" name="truename" id="truename" placeholder="输入真实姓名" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
										<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 邮箱：  </label>
						<div class="col-sm-10">
							<input type="email" name="email" id="email" placeholder="输入电子邮箱" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>
										<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 手机号码：  </label>
						<div class="col-sm-10">
							<input type="text" name="mobile" id="mobile" placeholder="输入手机号码" class="col-xs-10 col-sm-4" required/>
						</div>
					</div>
					<div class="space-4"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 是否启用： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<input name="status" id="status" value="1" checked="checked" class="ace ace-switch ace-switch-4 btn-flat" type="checkbox" />
							<span class="lbl">&nbsp;默认开启</span>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right" for="form-field-1"> 管理组： </label>
						<div class="col-sm-10" style="padding-top:5px;">
							<select name="group_id" id="group_id">
							<option value="0">请选择管理组</option>
								<?php foreach ($lists as $key => $value){ ?>
								<option value="<?php echo $value['id'];?>"><?php echo $value['title'];?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="space-4"></div>
					
					<div class="clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								保存
							</button>

							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								重置
							</button>
						</div>
					</div>
				</form>
				

					
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
{/block}
