<?php
return [
	'uplaod' => 'uploads',
	'template' => [
		'view_suffix' => 'php',
	],
	'paginate' => [
		'type' => 'bootstrap',
		'var_page' => 'page',
	],
	'check_email_time' => 60*30, //验证邮件时间（分钟）
	'DB_BACKUP_PATH' => ROOT_PATH.'backdb'.DS,
	'SQLFILESIZE' => 500000000,
	// o2o域名
	'YAH_web' => 'http://hdo2o.yao2099.com',
	// o2o接收图片域名
	'YAH_web_upload' => 'http://hdo2o.yao2099.com/index.php?m=Admin&c=Users&a=save_upload',
	'PAGE_SIZE' => 15,
	'view_replace_str' => [
		'__PUBLIC__'=>'/public',
		'__JS__' => '/public/admin/js',
		'__CSS__' => '/public/admin/css',
		'__IMG__' => '/public/admin/image',
		'__ROOT__' => '/',
	],
	'AUTH_CONFIG' => array(
		'NO_AUTH_USER' => array(2),
		'AUTH_ON' => true,
		'AUTH_USER' => config('database.prefix').'admin',
		'NO_AUTH_URL' => array(),
	),
		
		'web_log' =>
		array (
				'weblog_on' => false,
				'not_log_module' =>
				array (
						0 => 'install',
				),
				'not_log_controller' =>
				array (
						0 => 'home/Error',
						1 => 'home/Token',
						2 => 'admin/Ajax',
						3 => 'admin/Error',
						4 => 'admin/Ueditor',
						5 => 'admin/WebLog',
				),
				'not_log_action' =>
				array (
				),
				'not_log_data' =>
				array (
				),
				'not_log_request_method' =>
				array (
						0 => 'GET',
				),
		),
];