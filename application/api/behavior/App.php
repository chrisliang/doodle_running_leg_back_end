<?php
namespace app\api\behavior;

use think\Request;

class App {
	
	//初始化
	public function appInit(&$params){
		
	}
	
	public function run(&$params){
		$request = Request::instance();
		
		$token= $request->param('token');
		if (empty($token) || $token != config('token')) ajaxReturn(array('status' => 'error','msg' => 'Token check fail'));
		//echo date('Y-m-d H:i:s',1502942399186/1000);
		return true;
	}
	
	public function appEnd(&$params){
		
	}
	
}

