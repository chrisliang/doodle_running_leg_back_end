<?php
namespace app\api\controller;
// use think\Controller;
// use think\Db;
// use think\Request;
use think\Db;
use app\admin\model\Orders;
class User extends Common {
	private $token;
	private $userinfo;

	private $db = null;
	private $config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $orderField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
	}

	public function fuserinfo(){
		$this->is_validate();
		$model = db('user');
		$user_id = $this->userinfo['id'];
		$user = $model->where('id',$user_id)->find();
		$oto_model =  $this->db->name('orders');
		// 用户名
		$username = $user['username'];
		// 用户真实姓名
		$true_name = $user['truename'];
		// 头像
		$user_face = $user['user_face'];
		$openid = $user['openid'];

		$imgInfo = explode('http', $user_face);
		if (count($imgInfo)>=2) {
			$isThirdImage = 1;
		}else{
			$isThirdImage = 0;
		}
		// 是否认证
		$check_status = $user['check_status'];
		// 是否接单
		$is_job = $user['is_job'];
		// 唐峰康注释掉 【想不明白为什么重新修改！！！！】
		// if ($check_status == 1) {
		// 	$is_job = 1;
		// 	$model->where('Id',$user_id)->setField('is_job',1);
		// }else if($check_status == 0){
		// 	$is_job = 0;
		// 	$model->where('Id',$user_id)->setField('is_job',0);
		// }
		// 完成订单量
		$orders = db('delivery_order')->where(array('userId'=>$user_id,'status'=>3))->select();
		$order_num = count($orders);
		// 没有超时订单量
		$orders_s = db('delivery_order')->where(array('userId'=>$user_id,'status'=>3,'timeout'=>1))->select();
		$ss = count($orders_s);

		// 准时到达率
		if ($order_num<=0) {
			// 唐峰康改 到达率 默认都是 100%；
			$is_punctual = '100%';
		}else{
			$is_punctual = (round(($ss/$order_num)*100,0)).'%';
		}
		// 唐峰康改 用户评价 默认都是 5星
		$evaluate = 5;

		// 唐峰康改 计算用户评价
		// 1.status = 3 star_num !=0 的订单
		$delivery_order_info = db('delivery_order')->where("userId = '{$user_id}' and status = 3 and star_num > 0")->select();
		$delivery_order_count = count($delivery_order_info);
		// 总分数
		$total_num = 0;
		if ($delivery_order_count > 0) {
			foreach ($delivery_order_info as $k => $v) {
				$total_num +=$v['star_num'];
			}
			// 只保留整数
			$evaluate = intval($total_num/$delivery_order_count);
		}
		

		ajaxReturn(array(
				'status' => 'success',
				'data' => array(
						'name' => $username,
						'true_name' => $true_name,
						'user_face' => $user_face,
						'isThirdImage' => $isThirdImage,
						'check_status' => $check_status,
						'is_job' => $is_job,
						'openid' => $openid,
						'order_num' => $order_num,
						'is_punctual' => $is_punctual,
						'evaluate' => $evaluate,
						'car' => $this->userinfo['car']
						)
		));
		
	}
	public function f_is_job(){
		$this->is_validate();
		$user_id = $this->userinfo['id'];
		$model = db('user');
		$user = $model->where('id',$user_id);
		$status = $this->request->post('status');
		if ($status==1) {
			$user->update(['is_job'=>1]);
			ajaxReturn(array('status' => 'success','msg' => "接单中"));
		}else{
			$user->update(['is_job'=>0]);
			ajaxReturn(array('status' => 'success','msg' => "不接单"));
		}
	}
	// 消息，通知
	public function f_user_notice(){
		$this->is_validate();
		$user_id = $this->userinfo['id'];
		$model = db('user_msg');
		$model_order = db('delivery_order');
		$user_notices = $model->where(array('uid'=>$user_id))->where('status','<>',1)->order('id DESC')->select();
		if (empty($user_notices)) {
			ajaxReturn(array('status' => '2')); //无消息
		}
		$issue_model = db('issue');
		$data = array();
		foreach ($user_notices as $user_notice) {
			// 订单id
			$id = $user_notice['orderid'];
			$orderId = $model_order->where('id',$id)->value('orderId');
			// 问题类型id
			$issue_id = $user_notice['type'];
			$issue = $issue_model->where('id',$issue_id)->find();
			// 问题type
			$issue_type = $issue['type'];

			// 消息标题
			$notice_title = $issue['title'];
			// 消息内容
			$notice_content = $user_notice['content'];
			// 时间
			$time = $user_notice['time'];
			// 唐峰康改 获取消息发送的类型 有订单不结算，更改配送员，更改配送员的在前端是不能查看详情的
			$is_change = $model_order->where('id',$id)->value('is_change');
			$data[] = array("orderId"=>$orderId,"notice_title"=>$notice_title,"notice_content"=>$notice_content,'id'=>$id,'is_change'=>$is_change);

		}
		ajaxReturn(array('status' => 'success','data'=>$data));
	}
	public function f_change_notice(){
		$this->is_validate();
		$user_id = $this->userinfo['id'];
		$orderIds = $this->request->post('orderIds');
		$orderIds = explode(',',$orderIds);
		$model = db('user_msg');
		$model_order = db('delivery_order');
		foreach ($orderIds as $orderId) {
			$id = $model_order->where(array('orderId'=>$orderId))->value('id');
			$model->where(array('orderid'=>$id))->setField('status',1);
		}

		ajaxReturn(array('status' => 'success','msg'=>$orderIds));
	}
	// 头像
	public function user_upload(){
		$this->is_validate();
		$user_id = $this->userinfo['id'];
		// p(db('user')->where('id',$user_id)->value('user_face'));
		// exit();
		$user_face = '';
		$userFace = request()->file("user_face");
		// ajaxReturn(array('status' => 'success','msg' => $user_face));
		// 移动到框架应用根目录/public/uploads/ 目录下
		if ($userFace){
			$info = $userFace->move(ROOT_PATH . DS . config('uplaod') .DS.'face');
			if ($info){
				$user_face = config('uplaod').'/face/'.str_replace('\\', '/', $info->getSaveName());
				db('user')->where('id',$user_id)->update(['user_face'=>$user_face]);
			}else{
				$this->ajaxReturn(array('status' => 2,'msg' => '上传失败'));
			}
		}
		
		$data['user_face'] = $user_face;
		ajaxReturn(array('status' => 'success','data' => $data));
		// return $data;
	}
	// 申请身份验证
	public function f_application(){
		$this->is_validate();
		$user_id = $this->userinfo['id'];
		$true_name = $this->request->post('true_name');
		$id_card = $this->request->post('id_card');
		$thumb = '';
		$thumb2 = '';
		$thumb3 = '';
		$cardThumb = request()->file("card_thumb");
		if ($cardThumb){
			//移动到框架应用根目录/public/uploads/ 目录下
			$card_thumb_file = $cardThumb->move(ROOT_PATH . DS . config('uplaod') .DS.'idcard');
			if ($card_thumb_file){
				$thumb = config('uplaod').'/idcard/'.str_replace('\\', '/', $card_thumb_file->getSaveName());
			}else{
				$this->ajaxReturn(array('status' => 2,'msg' => '上传失败'));
			}
		}
		$cardThumb2 = request()->file("card_thumb2");
		if ($cardThumb2){
			// 移动到框架应用根目录/public/uploads/ 目录下
			$card_thumb_file2 = $cardThumb2->move(ROOT_PATH . DS . config('uplaod') .DS.'idcard');
			if ($card_thumb_file2){
				$thumb2 = config('uplaod').'/idcard/'.str_replace('\\', '/', $card_thumb_file2->getSaveName());
			}else{
				$this->ajaxReturn(array('status' => 2,'msg' => '上传失败'));
			}
		}
		$cardThumb3 = request()->file("card_thumb3");
		if ($cardThumb3){
			// 移动到框架应用根目录/public/uploads/ 目录下
			$card_thumb_file3 = $cardThumb3->move(ROOT_PATH . DS . config('uplaod') .DS.'idcard');
			if ($card_thumb_file3){
				$thumb3 = config('uplaod').'/idcard/'.str_replace('\\', '/', $card_thumb_file3->getSaveName());
			}else{
				$this->ajaxReturn(array('status' => 2,'msg' => '上传失败'));
			}
		}
		$data['card_thumb'] = $thumb;
		$data['card_thumb2'] = $thumb2;
		$data['card_thumb3'] = $thumb3;
		$data['car'] = $this->request->post('car');
		db('user')->where('id',$user_id)->update(['car' => $data['car'],'truename'=>$true_name,'id_card'=>$id_card,'card_thumb'=>$thumb,'card_thumb2'=>$thumb2,'card_thumb3'=>$thumb3]);
		ajaxReturn(array('status' => 'success','data' => $data));
		// return $data;
	}

	private function is_validate(){
		if (!request()->isPost()) ajaxReturn(array('status' => 'error','msg' => '网络错误'));
		$token = $this->request->param('token');
		if ($token != config('token')) ajaxReturn(array('status' => 'error','msg' => '网络错误'));
		$user_id = $this->request->post('userId');
		$user_id = _encrypt($user_id,'DECODE');
		$userinfo = db('user')->where(array('id'=>$user_id))->find();
		// if (empty($userinfo)) {
		// 	ajaxReturn(array('status' => 'error','msg' => '您未认证，请先认证。'));
		// }
		$this->token = $token;
		$this->userinfo = $userinfo;
	}

	// 用户绑定openid
	public function binding_wechat_openid(){
		if (!request()->isPost()) ajaxReturn(array('status' => 0,'msg' => '网络错误'));
    	$user_id = intval($this->request->post('user_id'));
    	$openid = $this->request->post('open_id');
    	$db_config = config('database.db_config');
    	$db = \Think\Db::connect($db_config);
    	$rs = $db->name('users')->where(array('userId' => $user_id))->setField('openid', $openid);
    	if ($rs !==false) {
    		ajaxReturn(array('status' => 1,'data' => array(
    		'user_id' => $user_id,
    		'openid' => $openid
    		)));
    	}else{
    		ajaxReturn(array('status' => -1));
    	}

	}

	// 用户取消微信绑定openid
	public function cancel_binding_wechat_openid(){
		if (!request()->isPost()) ajaxReturn(array('status' => 0,'msg' => '网络错误'));
    	$user_id = intval($this->request->post('user_id'));
    	$db_config = config('database.db_config');
    	$db = \Think\Db::connect($db_config);
    	$rs = $db->name('users')->where(array('userId' => $user_id))->setField('openid', 0);
    	if ($rs !==false) {
    		ajaxReturn(array('status' => 1,
    		));
    	}else{
    		ajaxReturn(array('status' => -1));
    	}

	}

	// 配送员绑定openid
	public function diliveryman_binding_wechat_openid(){
		// $this->post_check();
		$this->is_validate();
		$userId = $this->userinfo['id'];
		$openid = $this->request->post('openId');
    	$rs = db('user')->where(array('id' => $userId))->setField('openid', $openid);
    	if ($rs !==false) {
    		ajaxReturn(array('status' => 1,'data' => array(
    		'user_id' => $userId,
    		'openid' => $openid
    		)));
    	}else{
    		ajaxReturn(array('status' => -1));
    	}

	}

	// 配送员取消微信绑定openid
	public function diliveryman_cancel_binding_wechat_openid(){
		// $this->post_check();
		$this->is_validate();
		$userId = $this->userinfo['id'];
    	$rs = db('user')->where(array('id' => $userId))->setField('openid', '');
    	if ($rs !==false) {
    		ajaxReturn(array('status' => 1));
    	}else{
    		ajaxReturn(array('status' => -1));
    	}

	}
}
?>
