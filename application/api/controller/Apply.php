<?php
namespace app\api\controller;

class Apply extends Common {
	private $token;
	private $userinfo;


	public function f_get_data(){
		$this->is_validate();
		$model = db('apply');
		$user_id = $this->userinfo['id'];
		$page = $this->request->post('page');
		$orders = $model->where(array('userId'=>$user_id))->limit((10 * $page) . ",10")->order('id desc')->select();

		// 余额
		$money = db('user')->where('id',$user_id)->value('money');
		if ($money<0) {
			$money=0;
		}
		// 默认银行卡
		$card = db('bank_cards')->where(array('userId'=>$user_id,'is_default'=>1))->find();
		if (!empty($card)) {
		// $cardData = array();
			// id
			$card_id = $card['id'];
			// 银行名
			$bank_name = $card['bank_name'];
			// 银行卡号
			$bank_num = $card['bank_num'];
			//截取银行卡号前4位
			// $prefix = substr($bank_num,0,4);
			//截取银行卡号后4位
			$suffix = substr($bank_num,-4,4);
			$bank_num = " **** **** **** ".$suffix;
			$cardData = array('card_id' => _encrypt($card_id),'bank_name'=>$bank_name,'bank_num'=>$bank_num);
		}else{
			$cardData = 0;
		}
		
		// 没有订单
		if (empty($orders)) {
			ajaxReturn(array('status'=>'2','cardData'=>$cardData,'money'=>$money));
		}
		// 最小提现金额
		$applyMin = 1;
		$num = count($orders);
		$data = array();
		foreach ($orders as $order) {
			// 订单号
			$orderId = $order['orderId'];
			// 提现金额
			$apply_price = $order['apply_price'];
			// 时间
			$time = date('Y.m.d',$order['time']);
			// 状态
			$status = $order['status'];
			if ($status==1) {
                $status = "处理中";
            }else if ($status==2) {
                $status = "已完成";
            }else if ($status==3) {
                $status = "已拒绝";
            }else {
                $status = "待处理";
            }
			$data[] = array('orderId'=>$orderId,'apply_price'=>$apply_price,'time'=>$time,'status'=>$status);
		}
		ajaxReturn(array('status'=>'1','cardData'=>$cardData,'money'=>$money,'applyMin'=>$applyMin,'num'=>$num,'data'=>$data));

	}

	public function f_set_data(){
		$this->is_validate();
		$model = db('apply');
		$user_id = $this->userinfo['id'];
		$bank_id = $this->request->post('bankId');
		$bank_id = _encrypt($bank_id,'DECODE');
		$apply_price = $this->request->post('money');
		$user = db('user')->where('id',$user_id)->find();
		// 最小提现
		if ($apply_price < 1) {
			ajaxReturn(array('status'=>6,'applyMin'=>1));
		}
		// 余额不足
		$money = $user['money'];
		if ($apply_price > $money) {
			ajaxReturn(array('status'=>5));
		}
		// 银行卡不存在
		$user_bnak = db('bank_cards')->where(array('id'=>$bank_id,'userId'=>$user_id))->find();
		if (empty($user_bnak)) {
			ajaxReturn(array('status'=>2));
		}
		// 银行名

		$time = time();
		// 订单id
		$order_id = date('Y',$time);
		$order_id = substr($order_id, -2);
		$order_id .= date('m',$time);
		$order_id .= date('d',$time);
		$id = $model->max('id');
		$id = $id+1;
		$order_id .= sprintf("%04d", $id);


		$data = ["orderId"=>$order_id,"userId"=>$user_id,"mobile"=>$user['mobile'],"bank_user_name"=>$user['truename'],"bank_name"=>$user_bnak['bank_name'],"bank_access"=>$user_bnak['bank_access'],"bank_num"=>$user_bnak['bank_num'],"apply_price"=>$apply_price,"remark"=>"无","status"=>'0',"time"=>$time];
		$model->insert($data);
		db('user')->where('id',$user_id)->update(['money'=>$money-$apply_price]);
		
		ajaxReturn(array('status'=>1));

	}

	private function is_validate(){
		if (!request()->isPost()) ajaxReturn(array('status' => 'error','msg' => '网络错误'));
		$token = $this->request->param('token');
		if ($token != config('token')) ajaxReturn(array('status' => 'error','msg' => '网络错误'));
		$user_id = $this->request->post('userId');
		$user_id = _encrypt($user_id,'DECODE');
		$userinfo = db('user')->where(array('id'=>$user_id,'check_status'=>1))->find();
		if (empty($userinfo)) {
			ajaxReturn(array('status' => 'error','msg' => '您未认证，请先认证。'));
		}
		$this->token = $token;
		$this->userinfo = $userinfo;
	}
}

?>