<?php
namespace app\api\controller;

use think\db\Query;

class Index extends Common {
	
    public function index(){
    	/*
    	 * array (
			  'lng' => '113.316894',
			  'lat' => '23.139457',
			)
    	 */
    	
    	/*
    	function rad($d){
    		return $d * 3.1415926535898 / 180.0;
    	}
    	function GetDistance($lat1, $lng1, $lat2, $lng2){
    		$EARTH_RADIUS = 6378.137;
    		$radLat1 = rad($lat1);
    		//echo $radLat1;
    		$radLat2 = rad($lat2);
    		$a = $radLat1 - $radLat2;
    		$b = rad($lng1) - rad($lng2);
    		$s = 2 * asin(sqrt(pow(sin($a/2),2) +
    				cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)));
    		$s = $s *$EARTH_RADIUS;
    		$s = round($s * 10000) / 10000;
    		return $s;
    	}
    	*/
    	/**
    	 * @desc 根据两点间的经纬度计算距离
    	 * @param float $lat 纬度值
    	 * @param float $lng 经度值
    	 */
//     	function getDistance($lat1, $lng1, $lat2, $lng2){
//     		$earthRadius = 6367000;
//     		$lat1 = ($lat1 * pi() ) / 180;
//     		$lng1 = ($lng1 * pi() ) / 180;
    		
//     		$lat2 = ($lat2 * pi() ) / 180;
//     		$lng2 = ($lng2 * pi() ) / 180;
//     		$calcLongitude = $lng2 - $lng1;
//     		$calcLatitude = $lat2 - $lat1;
//     		$stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
//     		$stepTwo = 2 * asin(min(1, sqrt($stepOne)));
//     		$calculatedDistance = $earthRadius * $stepTwo;
//     		return round($calculatedDistance / 1000,2);
//     		//return round($calculatedDistance * 10000) / 10000;
//     	} 
//     	p(GetDistance(113.32212,24.147166,113.323737,23.14602));
    	
    	$startLng = round($this->request->post('lng'),2);    	
    	$endLng = sprintf("%.3f",substr(sprintf("%.5f", $this->request->post('lng')), 0, -2));
    	$startLat = round($this->request->post('lat'),2);
    	$endLat = sprintf("%.4f",substr(sprintf("%.6f", $this->request->post('lat')), 0, -2));
    	//$where = "(lng<'{$startLng}' and lng>'{$endLng}') and (lat<'{$startLat}' and lat>'{$endLat}')";
    	$where = "status=1 and check_status=1 and `lng`<'{$startLng}' and `lng`>'{$endLng}' and lng != ''";
    	$data = db('user')->where($where)->count();
        ajaxReturn(array('status' => 1,'count' => $data,'msg' => 'ok'));
    }
    
    //coupon列表
    public function couponlist(){
    	if (!request()->isPost()) ajaxReturn(array('status' => 0,'msg' => '网络错误'));
    	$user_id = intval($this->request->post('user_id'));
    	$prefix = config('database.prefix');
    	$data = db()->query("select uc.*,u.endtime from {$prefix}coupon_use uc inner join {$prefix}coupon u ON uc.coupon_id=u.id where uc.uid={$user_id} order by uc.time desc");
    	foreach ($data as $key => $value){
    		$data[$key]['endtime'] = date('Y-m-d',$value['endtime']);
    		if (time() > $value['endtime']){
    			$data[$key]['vail'] = 0; //无效
    		}else{
    			$data[$key]['vail'] = 1;
    		}
    	}
    	ajaxReturn(array('status' => 1,'data' => $data,'msg' => 'ok'));
    }
    
    public function myd(){
    	if (!request()->isPost()) ajaxReturn(array('status' => 0,'msg' => '网络错误'));
    	$user_id = intval($this->request->post('user_id'));
    	$prefix = config('database.prefix');
    	$data = db()->query("select uc.*,u.endtime from {$prefix}coupon_use uc inner join {$prefix}coupon u ON uc.coupon_id=u.id where uc.uid={$user_id} order by uc.time desc");
    	$db_config = config('database.db_config');
    	$db = \Think\Db::connect($db_config);
    	$user = $db->name('users')->where(array('userId' => $user_id))->find();
    	$user['userMoney'] = subMoney($user['userMoney']);
    	ajaxReturn(array('status' => 1,'data' => array(
    		'coupon_num' => count($data),
    		'userinfo' => $user
    	)));
    }
    
    
}
