<?php
namespace app\api\controller;

use think\Controller;
use think\Hook;

class Common extends Controller {
	
	protected function _initialize(){
		header("Content-Type:text/html;charset=utf-8");
		parent::_initialize();
		
		Hook::add('app_init', 'app\api\behavior\\App');
		Hook::add('run', 'app\api\behavior\\App');
		Hook::add('app_end', 'app\api\behavior\\App');
		
		Hook::listen('app_init',$params);
		Hook::listen('run',$params);
		Hook::listen('app_end',$params);
		
	}
	
	protected function ajaxReturn($data){
		exit(json($data)->getContent());
	}
	
}