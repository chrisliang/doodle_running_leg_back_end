<?php
namespace app\api\controller;
use think\Db;
use app\admin\model\Orders;
class Cards extends Common {
	private $token;
	private $userinfo;

	private $db = null;
	private $config = '';
	private $oto_db_prefix = ''; //oto平台数据库名和表前缀
	private $pt_table_prefix = ''; //跑腿平台表前缀
	private $orderField = '';
	protected function _initialize(){
		parent::_initialize();
		$this->config = config('database.db_config');
		$this->oto_db_prefix = $this->config['database'].'.'.$this->config['prefix'];
		$this->pt_table_prefix = config('database.prefix');
		$this->db = Db::connect($this->config);
	}

	public function f_my_cards(){
		$this->is_validate();
		$arr_cards = array();
		$model = db('bank_cards');
		$user_id = $this->userinfo['id'];
		$cards = $model->where(array('userId'=>$user_id))->select();
		foreach ($cards as $card) {
			// id
			$id = $card['id'];
			// 银行名
			$bank_name = $card['bank_name'];
			// 银行卡号
			$bank_num = $card['bank_num'];
			//截取银行卡号前4位
			// $prefix = substr($bank_num,0,4);
			//截取银行卡号后4位
			$suffix = substr($bank_num,-4,4);
			$bank_num = " **** **** **** ".$suffix;
			$arr_cards[] = array('id' => _encrypt($id),'bank_name'=>$bank_name,'bank_num'=>$bank_num);
		}
		ajaxReturn(array('status' => 'success','data' => $arr_cards));
		
	}
	// 获取真实姓名和电话号码
	public function f_get_user_info(){
		$this->is_validate();
		$model = db('user');
		$user_id = $this->userinfo['id'];
		$user = $model->where(array('id'=>$user_id))->find();
		ajaxReturn(array('status' => 'success','data' => array('name'=>$user['truename'],'phone'=>$user['mobile'])));
	}
	// 获取支持的银行列表
	public function f_get_bank(){
		$arr_cards = array();
		$oto_banks = $this->db->name('banks')->select();

		foreach ($oto_banks as $oto_bank) {
			// 银行卡id
			$bankId = $oto_bank['bankId'];
			// 银行名
			$bank_name = $oto_bank['bankName'];
			$arr_banks[] = array('bankId' => _encrypt($bankId),'bank_name'=>$bank_name);
		}
		ajaxReturn(array('status' => 'success','data' => $arr_banks));
	}
	// 添加银行卡
	public function f_add_card(){
		$this->is_validate();
		$model = db('bank_cards');
		$user_id = $this->userinfo['id'];

		$bank_id = $this->request->post('bankId');
		$bank_id = _encrypt($bank_id,'DECODE');
		$bank_name = $this->request->post('bankName');
		$oto_bank = $this->db->name('banks')->where(array('bankId'=>$bank_id,'bankName'=>$bank_name))->find();
		if (empty($oto_bank)) {
			ajaxReturn(array('status' => 'error','msg' => '添加失败'));
		}
		$user_name = $this->request->post('userName');
		$user_phone = $this->request->post('userPhone');
		$userinfo = db('user')->where(array('id'=>$user_id,'truename'=>$user_name,'mobile'=>$user_phone))->find();
		if (empty($userinfo)) {
			ajaxReturn(array('status' => 'error','msg' => '添加失败'));
		}
		$bank_num = $this->request->post('card');
		$user_card = $model->where(array('userId'=>$user_id,'bank_num'=>$bank_num))->find();
		if (!empty($oto_card)) {
			ajaxReturn(array('status' => 'error','msg' => '此卡已存在'));
		}
		$other_card = $model->where(array('bank_num'=>$bank_num))->find();
		if (!empty($other_card)) {
			ajaxReturn(array('status' => 'error','msg' => '此卡已被其他用户绑定'));
		}
		$bank_access = $this->request->post('bankAccess');
		$data = ["userId"=>$user_id,"bank_user_name"=>$user_name,"bank_name"=>$bank_name,"bank_access"=>$bank_access,"bank_num"=>$bank_num,"mobile"=>$user_phone,"time"=>time()];
		$model->insert($data);
		ajaxReturn(array('status' => 'success','msg' => '添加成功'));
	}
	// 设置默认和删除
	public function f_operate_card(){
		$this->is_validate();
		$model = db('bank_cards');
		$user_id = $this->userinfo['id'];
		$bank_id = $this->request->post('bankId');
		$bank_id = _encrypt($bank_id,'DECODE');
		$user_card = $model->where(array('id'=>$bank_id,'userId'=>$user_id))->find();
		if (empty($user_card)) {
			ajaxReturn(array('status' => 'error','msg' => '操作失败'));
		}
		$status = $this->request->post('status');
		// 设置默认
		if ($status==1) {
			$model->where(array('userId'=>$user_id))->update(['is_default'=>0]);
			$model->where(array('id'=>$bank_id,'userId'=>$user_id))->update(['is_default'=>1]);
			ajaxReturn(array('status' => 'success','msg' => '设置成功'));
		}
		if ($status=='remove') {
			$model->where(array('id'=>$bank_id,'userId'=>$user_id))->delete();
			ajaxReturn(array('status' => 'success','msg' => '删除成功'));
		}
	}

	private function is_validate(){
		if (!request()->isPost()) ajaxReturn(array('status' => 'error','msg' => '网络错误'));
		$token = $this->request->param('token');
		if ($token != config('token')) ajaxReturn(array('status' => 'error','msg' => '网络错误'));
		$user_id = $this->request->post('userId');
		$user_id = _encrypt($user_id,'DECODE');
		$userinfo = db('user')->where(array('id'=>$user_id,'check_status'=>1))->find();
		if (empty($userinfo)) {
			ajaxReturn(array('status' => 'error','msg' => '您未认证，请先认证。'));
		}
		$this->token = $token;
		$this->userinfo = $userinfo;
	}
}
?>